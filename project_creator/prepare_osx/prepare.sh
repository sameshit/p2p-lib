cd ../../
if [ ! -d build ]
then
mkdir build
fi
cd build
if [ $# -eq 1 ]
then
	if [ $1 = "release" ]
	then
	echo "-- RELEASE BUILD"
	cmake -G Xcode -DCMAKE_BUILD_TYPE=Release ../src
	fi
else
echo "-- DEBUG BUILD"
cmake -G Xcode -DCMAKE_BUILD_TYPE=Debug ../src
fi
