cd ../../
if [ ! -d build ]
then
mkdir build
fi
cd build
if [ $# -eq 1 ]
then
	if [ $1 = "release" ]
	then
	echo "-- RELEASE BUILD"
	cmake -DCMAKE_BUILD_TYPE=Release ../src
	fi
	else
echo "-- DEBUG BUILD"
cmake -DCMAKE_BUILD_TYPE=Debug ../src
fi
