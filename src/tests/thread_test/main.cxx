#include "../../lib/core/CoreObject.h"

class ThreadTestClass
{
public:
	ThreadTestClass() {};
	~ThreadTestClass() {};
	void Call();
};


void ThreadTestClass::Call()
{
	fprintf(stderr,"hello\n");
	sleep(1);
}

int main()
{
	ThreadTestClass test_class;
	Thread *thread;
	
	fprintf(stderr,"before thread\n");
	thread = new Thread;
	fprintf(stderr,"thread created\n");
	thread->OnThread.Attach<ThreadTestClass>(&test_class,&ThreadTestClass::Call);
	sleep(2);
	fprintf(stderr,"end of sleep\n");
	delete thread;
	return 0;
}
