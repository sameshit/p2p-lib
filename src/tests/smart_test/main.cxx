#include "../../lib/core/CoreObject.h"

int main()
{
    SmartSortedList<uint32_t,uint16_t> some_set;
    SmartPosition<uint32_t, uint16_t>  *pos1,*pos2,*pos3,*pos4,*pos5,*pos6,*pos7,*pos8,*pos9,*pos10,*pos11,*pos12,*pos13,*tmp;
        
    pos1 = some_set.Insert(3, 1);
    pos2 = some_set.Insert(3, 1);
    pos3 = some_set.Insert(3, 1);
    pos4 = some_set.Insert(3, 1);    
    
    pos5 = some_set.Insert(0, 1);
    
    for (tmp = some_set.First(); tmp != NULL; tmp = tmp->GetNext())
        fprintf(stderr," %"PRIu32,tmp->GetKey());
    
    fprintf(stderr, "\n");
    
    pos5->UpdateKey(1);
    pos5->UpdateKey(2);    
    pos5->UpdateKey(3);    
    
    pos1->UpdateKey(4);
    pos2->UpdateKey(4);
    pos3->UpdateKey(4);

    for (tmp = some_set.First(); tmp != NULL; tmp = tmp->GetNext())
        fprintf(stderr," %"PRIu32,tmp->GetKey());    

    fprintf(stderr, "\n");
    
    return 0;
}