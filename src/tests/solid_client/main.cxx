#include "../../lib/core/CoreObject.h"
#include "../../lib/protocols/rmp/RMPNode.h"

using namespace RMP;

class Controller
{
public:
	Controller(CoreObject *core) { _core = core;}
	virtual ~Controller() {}
	
	void Connect(RMPSession *session)
	{
		LOG<<session->GetIp()<<":"<<session->GetPort()<<" connected"<<std::endl;
	}
	void Disconnect(RMPSession *session)
	{
		LOG<<session->GetIp()<<":"<<session->GetPort()<<" disconnected"<<std::endl;
	}
private:
	CoreObject *_core;
};

int main(int argc, char *argv[])
{
	CoreObject *_core;
	RMPNode *rmp_node;
    RMPSession *session;
    Controller *controller;
    
	_core = new CoreObject();
	fast_new1(RMPNode,rmp_node,_core);
	fast_new1(Controller,controller,_core);
    
    rmp_node->OnConnect.Attach<Controller>(controller,&Controller::Connect);
    rmp_node->OnDisconnect.Attach<Controller>(controller,&Controller::Disconnect);
    
    assert(rmp_node->Bind(12346));

	assert(rmp_node->Connect("127.0.0.1",12345) != NULL);
	
	ev_run(_core->GetLoop());

	fast_delete(Controller,controller);
	fast_delete(RMPNode,rmp_node);
	delete _core;
	return 0;
}
