#include "../../lib/core/protocols/TCP/TCPListener.h"

int main()
{
    CoreObject *_core = new CoreObject();
    TCPListener *listener;
    
    
    fast_new2(TCPListener,listener,_core, 8085);
    
    
    ev_run(_core->GetLoop());
    
    fast_delete(TCPListener, listener);
    delete _core;
    return 0;
}