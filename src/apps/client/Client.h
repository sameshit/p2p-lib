#ifndef CLIENT__H
#define CLIENT__H

#include "../../lib/tracker/TrackerClient.h"
#include "../../lib/protocols/p2pprotocol/P2PProtocol.h"
#include "../../lib/buffer/BufferManager.h"

class Client 
{
public:
    Client(CoreObject *core,const char *stream_name);
    virtual ~Client();
    
    void Stop(SolidUDPSession *session);
private:
    CoreObject          *_core;
    BufferManager       *buffer_manager;
    TrackerClient       *tracker_client;
    P2PProtocol         *p2p_protocol;
};

#endif