#include "Client.h"

int main()
{
    CoreObject  *_core;
    Client      *client;
    
    _core = new CoreObject();    
    fast_new2(Client,client,_core,"TestStream");
    
    ev_run(_core->GetLoop());
    
    fast_delete(Client,client);    
    delete _core;
    
    return 0;
}