#ifndef SERVERTRACKER__H
#define SERVERTRACKER__H

#include "../../lib/protocols/solidudp/SolidUDPProtocol.h"
#include "../../lib/tracker/TrackerMessages.h"
#include "AuthOperations.h"
#include <set>
#include "NeighborManager.h"


class AuthData
{
    public:
    AuthData() {}
    ~AuthData() {}
    uint16_t        p2p_port;
    std::string     stream_name;
};

class ServerTracker 
:public SolidUDPProtocol
{
public:    
    ServerTracker(CoreObject *core);
    virtual ~ServerTracker();
    
    void OnNewMessage(SolidUDPSession *from,uint8_t *data,ssize_t size);
    
    void OnStreamerAuthResult (SolidUDPSession *from, bool &result);
    
    void OnStreamerAuth (SolidUDPSession* from, uint8_t* data, ssize_t &size);    
    void OnClientAuth   (SolidUDPSession* from, uint8_t* data, ssize_t &size);
    void OnNewNeighbor  (SolidUDPSession* from, uint8_t* data, ssize_t &size);
    void OnOldNeighbor  (SolidUDPSession* from, uint8_t* data, ssize_t &size);
    
    void OnSessionDisconnecting(SolidUDPSession *);
private:
    AuthOperations      *_auth_operations;
    NeighborManager     *_neighbor_manager;     
    std::map<SolidUDPSession *,AuthData*> _auth_data_by_udp_session;
};

#endif
