#include "TrackerSession.h"

TrackerSession::TrackerSession(SolidUDPSession *session,uint16_t &p2p_port,TrackerSession *streamer)
{
    _p2p_port       = p2p_port;
    _udp_session    = session;    
    _activated      = false;
    
    if (streamer == NULL)
        _streamer_session = this;
    else
        _streamer_session = streamer;
}

TrackerSession::~TrackerSession()
{

}

void TrackerSession::AddNeighbor(uint32_t &network_ip,uint16_t &network_port)
{
    _neighbors.insert(std::make_pair(network_ip, network_port));
    OnNeighborCountChanged(this,_neighbors.size());
}

void TrackerSession::DeleteNeighbor(uint32_t &network_ip,uint16_t &network_port)
{
    std::multimap<uint32_t, uint16_t>::iterator it;
    
    it = _neighbors.find(network_ip);
    
    for (; it != _neighbors.end() && (*it).second != network_port; it ++);
    
    assert(it != _neighbors.end());
    _neighbors.erase(it);
    
    OnNeighborCountChanged(this,_neighbors.size());
}

bool TrackerSession::NeighborExists(uint32_t network_ip, uint16_t network_port)
{
    std::multimap<uint32_t,uint16_t>::iterator it;
    
    it = _neighbors.find(network_ip);
    
    if (it == _neighbors.end())
        return false;
    
    for (; it != _neighbors.end() && (*it).second != network_port; it ++);
    
    if (it == _neighbors.end())
        return false;
    
    return true;
}