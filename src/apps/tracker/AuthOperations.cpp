#include "AuthOperations.h"

using namespace std;

AuthOperations::AuthOperations(CoreObject *core,const char *hiredis_host, uint16_t hiredis_port)
:HiredisConnection(core,hiredis_host,hiredis_port)
{        
}

AuthOperations::~AuthOperations()
{
    
}

void AuthOperations::StreamerAuth(SolidUDPSession *session,std::string &stream_name)
{            
    redisAsyncCommand(_redis_ctx, StreamerAuthStreamExists, session, "SADD streams %s",stream_name.c_str());
}

void AuthOperations::StreamerAuthStreamExists(redisAsyncContext *c, void *preply, void *privdata)
{
    AuthOperations      *authoperations = static_cast<AuthOperations*>((HiredisConnection*)c->data);
    SolidUDPSession     *session        = (SolidUDPSession *)privdata;
    redisReply          *reply          = (redisReply*)preply;
    bool                success;
    
    success = reply->integer == 1;
    
    authoperations->OnStreamerAuth(session,success);
        
}

void AuthOperations::DeleteStream(std::string &stream_name)
{
    redisAsyncCommand(_redis_ctx,NULL,NULL,"SREM streams %s",stream_name.c_str());
}