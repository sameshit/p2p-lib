#ifndef NEIGHBORMANAGER__H
#define NEIGHBORMANAGER__H

#include "StreamManager.h"

#define NEIGHBORMANAGER_DEFAULTNEIGHBORCOUNT 3

class NeighborManager 
:public StreamManager
{
public:
    NeighborManager(CoreObject *core);
    virtual ~NeighborManager();    
private:
    ev_timer _neighbor_timer;
    static void NeighborTimerTimeout (struct ev_loop *loop, ev_timer *w, int revents);
};

#endif