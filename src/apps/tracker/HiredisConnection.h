#ifndef HIREDISCONNECTION__HH
#define HIREDISCONNECTION__HH

#include "../../lib/core/CoreObject.h"
#include <hiredis.h>
#include <async.h>
#include <adapters/libev.h>

class HiredisConnection
{    
public:
    HiredisConnection(CoreObject *core,const char *hostname="127.0.0.1",uint16_t port = 6379);
    ~HiredisConnection();
private:    
    static void DisconnectionCallback(const redisAsyncContext *c, int status);
    static void ConnectionCallback   (const redisAsyncContext *c,int status);    
protected:
    CoreObject *_core;
    redisAsyncContext *_redis_ctx;
};

#endif
