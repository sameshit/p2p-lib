#ifndef STREAMMANAGER__H
#define STREAMMANAGER__H

#include "Stream.h"

class StreamManager 
{
public:
    StreamManager(CoreObject *core);
    virtual ~StreamManager();
    
    void            CreateStream           (SolidUDPSession *session,uint16_t &p2p_port,std::string &stream_name);    
    bool            AddClientToStream      (SolidUDPSession *session,uint16_t &p2p_port,std::string &stream_name);
    bool            RemoveClient           (SolidUDPSession *session);
    TrackerSession* FindTrackerSession     (SolidUDPSession *session);
    Stream*         FindStream             (std::string &stream_name);
    
    Event1<std::string &> OnStreamClosing;    
protected:
    std::map<std::string,Stream *>                  _streams_by_name;
    CoreObject                                      *_core;
};

#endif