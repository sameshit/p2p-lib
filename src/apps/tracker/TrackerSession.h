#ifndef TRACKERSESSION_H
#define TRACKERSESSION_H

#include "../../lib/protocols/solidudp/SolidUDPProtocol.h"

class TrackerSession
{
public:
    TrackerSession  (SolidUDPSession *session,uint16_t &p2p_port,TrackerSession *streamer = NULL);
    ~TrackerSession ();    
    
    void                        AddNeighbor     (uint32_t &network_ip,uint16_t &network_port);
    void                        DeleteNeighbor  (uint32_t &network_ip,uint16_t &network_port);
    inline SolidUDPSession*     GetUDPSession() {return _udp_session;}
    inline uint16_t             GetP2PPort()    {return _p2p_port;}
    bool                        NeighborExists  (uint32_t network_ip,uint16_t network_port);
private:
    uint16_t                                    _p2p_port;
    SolidUDPSession                             *_udp_session;
    TrackerSession                              *_streamer_session;
    bool                                        _activated;
    std::multimap<uint32_t, uint16_t>           _neighbors;
    SmartPosition<uint32_t, TrackerSession *>   *_stream_position;
    
    Event2<TrackerSession *,uint32_t> OnNeighborCountChanged;
    
    friend class Stream;
};

#endif