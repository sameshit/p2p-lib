#include "StreamManager.h"

StreamManager::StreamManager(CoreObject *core)
{
    _core = core;
}

StreamManager::~StreamManager()
{
    std::map<std::string,Stream *>::iterator it;
    
    for (it = _streams_by_name.begin(); it != _streams_by_name.end(); it ++)
    {
        LOG << "Deleting stream "<<(uint64_t)(*it).second << std::endl;
        fast_delete(Stream, (*it).second);    
    }
}


void StreamManager::CreateStream(SolidUDPSession *session, uint16_t &p2p_port, std::string &stream_name)
{
    Stream *stream;
    
    fast_new4(Stream, stream, _core, session, p2p_port, stream_name);
    
    _streams_by_name.insert(std::make_pair(stream_name, stream));
    
        
    LOG << "Created new stream with name: "<< stream_name.c_str()<<" ("<< (uint64_t)stream << std::endl;
}

bool StreamManager::AddClientToStream(SolidUDPSession *session, uint16_t &p2p_port, std::string &stream_name)
{
    Stream          *stream;
    TrackerSession  *tracker_session;
    std::map<std::string,Stream *>::iterator it;
    
    it = _streams_by_name.find(stream_name); 
        
    if (it == _streams_by_name.end())
        return false;
    
    stream = (*it).second;    
    tracker_session = stream->AddSession(session,p2p_port);
    
    return true;
}

bool StreamManager::RemoveClient(SolidUDPSession *session)
{
    Stream *stream;
    std::map<std::string,Stream*>::iterator it;
    TrackerSession *tracker_session;
    
    for (it = _streams_by_name.begin(); it != _streams_by_name.end(); it++)
    {
        stream = (*it).second;
        
        tracker_session = stream->Find(session);
        if (tracker_session != NULL)
        {
            if (tracker_session != stream->_streamer)                        
                stream->DeleteSession(session);
            else
            {
                OnStreamClosing(stream->_stream_name);
                _streams_by_name.erase(it);
                fast_delete(Stream, stream);
            }
            return true;
        }
    }
    
    return false;
}

TrackerSession* StreamManager::FindTrackerSession(SolidUDPSession *session)
{
    std::map<std::string,Stream*>::iterator it;
    TrackerSession *tracker_session;
    Stream *stream;
    
    for (it = _streams_by_name.begin(); it != _streams_by_name.end(); it ++)
    {
        stream = (*it).second;
        tracker_session = stream->Find(session);
        if (tracker_session != NULL)
            return tracker_session;
    }
    
    return NULL;
}

Stream* StreamManager::FindStream(std::string &stream_name)
{
    std::map<std::string,Stream *>::iterator it;
    
    it = _streams_by_name.find(stream_name);
    if (it == _streams_by_name.end())
        return NULL;
    else
        return (*it).second;

}
