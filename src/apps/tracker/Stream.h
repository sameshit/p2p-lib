#ifndef STREAM__H
#define STREAM__H

#include "TrackerSession.h"




class Stream
{
public:
    Stream(CoreObject *core,SolidUDPSession *streamer,uint16_t &p2p_port,std::string stream_name);
    ~Stream();    
    TrackerSession*     AddSession     (SolidUDPSession *session,uint16_t &p2p_port);
    void                DeleteSession  (SolidUDPSession *session);
    TrackerSession*     Find         (SolidUDPSession *session);

    inline TrackerSession* GetStreamer() {return _streamer;}
private:
    SmartSortedList<uint32_t, TrackerSession *>     _clients_by_neighbors_count;
    std::string                                     _stream_name;
    CoreObject                                      *_core;
    TrackerSession                                  *_streamer;
    std::map<SolidUDPSession *,TrackerSession *>    _clients_by_solid_session;
    
    void OnNeighborCountChanged(TrackerSession *tracker_session,uint32_t new_size);
    
    friend class StreamManager;
    friend class NeighborManager;
};

#endif