#include "HiredisConnection.h"

HiredisConnection::HiredisConnection(CoreObject *core,const char *hostname,uint16_t port)
{
    _core = core;
    _redis_ctx = redisAsyncConnect(hostname, port);
    
    redisLibevAttach(_core->GetLoop(), _redis_ctx);
    _redis_ctx->data = this;
    
    redisAsyncSetDisconnectCallback(_redis_ctx,HiredisConnection::DisconnectionCallback);
    redisAsyncSetConnectCallback(_redis_ctx, HiredisConnection::ConnectionCallback);
}

HiredisConnection::~HiredisConnection()
{
    redisAsyncDisconnect(_redis_ctx);
}

void HiredisConnection::DisconnectionCallback(const redisAsyncContext *c, int status)
{
    HiredisConnection   *hiredis = (HiredisConnection*)c->data;
    CoreObject          *_core   = hiredis->_core;
    LOG << "Hiredis connection dropped with status "<<status<<std::endl; 
}

void HiredisConnection::ConnectionCallback(const redisAsyncContext *c, int status)
{
    HiredisConnection   *hiredis = (HiredisConnection*)c->data;
    CoreObject          *_core   = hiredis->_core;
    LOG << "Connected to hiredis server with status "<<status<<std::endl; 
}