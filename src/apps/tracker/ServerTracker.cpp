#include "ServerTracker.h"
#include <string>

ServerTracker::ServerTracker(CoreObject *core)
:SolidUDPProtocol(core,TRACKERDEFAULTPORT)
{    
    fast_new1(AuthOperations,_auth_operations,core); 
    fast_new1(NeighborManager,_neighbor_manager,core);
    
    _auth_operations->OnStreamerAuth.Attach<ServerTracker>(this, &ServerTracker::OnStreamerAuthResult);
    
    OnDisconnect.Attach<ServerTracker>(this, &ServerTracker::OnSessionDisconnecting);
    _neighbor_manager->OnStreamClosing.Attach<AuthOperations>(_auth_operations, &AuthOperations::DeleteStream);

	NewMessageEvt.Attach<ServerTracker>(this,&ServerTracker::OnNewMessage);	
}

ServerTracker::~ServerTracker()
{
    fast_delete(NeighborManager, _neighbor_manager);
    fast_delete(AuthOperations, _auth_operations);
}

void ServerTracker::OnNewMessage(SolidUDPSession *from, uint8_t *data, ssize_t size)
{
    uint8_t *msg_data = &data[1];
    ssize_t msg_size = size - 1;
    
    
    switch (data[0]) 
    {
        case TRACKER_STREAMERAUTH:
            OnStreamerAuth(from,msg_data,msg_size);
        break;
        case TRACKER_CLIENTAUTH:
            OnClientAuth(from,msg_data,msg_size);
        break;
        default:            
            if (_neighbor_manager->FindTrackerSession(from) == NULL)
            {
                LOG<<"Unauthenticated session request from "<<from->GetRealHost()<<":"<<from->GetRealPort()<<std::endl;
                from->Disconnect();
                return;
            }
            
            switch (data[0])
            {
                case TRACKER_NEWNEIGHBOR:
                    OnNewNeighbor(from,msg_data,msg_size);
                break;
                case TRACKER_OLDNEIGHBOR:
                    OnOldNeighbor(from,msg_data,msg_size);
                break;
                default:
                    LOG<<"Unknown message received from "<<from->GetRealHost()<< ":" <<from->GetRealPort()<<std::endl;
                    from->Disconnect();
                break;
            }
        break;
    }
}

void ServerTracker::OnStreamerAuth(SolidUDPSession *from, uint8_t *data, ssize_t &size)
{
    AuthData                    *auth_data;
 
    fast_new(AuthData, auth_data);
        
    auth_data->p2p_port = Utils::GetBe16(data);
    auth_data->stream_name.assign((char*)&data[2],size-2);    
    
    _auth_data_by_udp_session.insert(std::make_pair(from, auth_data));    
    _auth_operations->StreamerAuth(from, auth_data->stream_name);
}

void ServerTracker::OnClientAuth(SolidUDPSession *from,uint8_t *data, ssize_t &size)
{
    uint16_t            p2p_port;
    std::string         stream_name;
	uint8_t				out_data[1];
    
    p2p_port = Utils::GetBe16(data);
    stream_name.assign((char*)&data[2],size-2);    
    
    if (_neighbor_manager->FindStream(stream_name) == NULL)
    {
        LOG << "Client authentication denied for "<<from->GetRealHost()<<":"<<from->GetRealPort()<<". Stream name request was: "<< stream_name<<std::endl;
		TrackerMessages::ClientAuthNoSuchStream(_core,out_data);
        from->Send(out_data,1);
        from->Disconnect();
    }
    else
    {
        _neighbor_manager->AddClientToStream(from, p2p_port, stream_name);
		TrackerMessages::ClientAuthOk(_core,out_data);
        from->Send(out_data,1);
        LOG << "New client connected (" <<from->GetRealHost()<<":"<<from->GetRealPort()<<")"<<std::endl;        
    }
}

void ServerTracker::OnNewNeighbor(SolidUDPSession *from,uint8_t *data,ssize_t &size)
{
    uint32_t ip;
    uint16_t port;
    TrackerSession *tracker_session;
    
    if (size != 6)
    {
        LOG << "Invalid NewNeighbor message (size is "<<size<<", but should be 6) from "<< from->GetRealHost()<<":"<< from->GetRealPort() <<". Disconnecting it"<< std::endl;
        from->Disconnect();
        return;
    }
    
    ip   = Utils::GetBe32(&data[0]);
    port = Utils::GetBe16(&data[4]);
    
    tracker_session = _neighbor_manager->FindTrackerSession(from);
    tracker_session->AddNeighbor(ip, port);
    
    
    LOG << "New neighbor call from "<<from->GetRealHost()<<":"<<from->GetRealPort()<<" with value:"<<ip<<":"<<port<<std::endl;
}

void ServerTracker::OnOldNeighbor(SolidUDPSession *from,uint8_t *data,ssize_t &size)
{
    uint32_t ip;
    uint16_t port;
    TrackerSession *tracker_session;
    
    if (size != 6)
    {
        LOG << "Invalid OldNeighbor message (size is "<<size <<", but should be 6) from "<<from->GetRealHost() <<":"<<from->GetRealPort() <<". Disconnecting it"<<std::endl;
        from->Disconnect();
        return;
    }
    
    ip   = Utils::GetBe32(&data[0]);
    port = Utils::GetBe16(&data[4]);
    
    tracker_session = _neighbor_manager->FindTrackerSession(from);
    tracker_session->DeleteNeighbor(ip, port);

    LOG << "Old neighbor call from " <<from->GetRealHost() <<":"<<from->GetRealPort() <<" with value:"<<ip<<":"<<port<<std::endl;
}

void ServerTracker::OnStreamerAuthResult(SolidUDPSession *from, bool &result)
{
    AuthData *auth_data;    
    std::map<SolidUDPSession *,AuthData *>::iterator it;
	uint8_t data[1];
    
    it = _auth_data_by_udp_session.find(from);
        
    if (it == _auth_data_by_udp_session.end())
    {
        LOG << "Streamer disconnected before getting auth result. Skipping him." << std::endl;
        return;
    }

    auth_data = (*it).second;
    
    if (result)
    {
        _neighbor_manager->CreateStream(from, auth_data->p2p_port, auth_data->stream_name);
		TrackerMessages::StreamerAuthOk(_core,data);
        from->Send(data,1);        
    }
    else
    {
		TrackerMessages::StreamerAuthDenied(_core,data);
        from->Send(data,1);
        from->Disconnect();
    }    
    fast_delete(AuthData    ,auth_data);
}

void ServerTracker::OnSessionDisconnecting(SolidUDPSession *session)
{
    std::map<SolidUDPSession*,AuthData*>::iterator it;
    
    _neighbor_manager->RemoveClient(session);    
    it = _auth_data_by_udp_session.find(session);
    if (it != _auth_data_by_udp_session.end())
        _auth_data_by_udp_session.erase(it);
}
