#include "Stream.h"

Stream::Stream(CoreObject *core,SolidUDPSession *streamer,uint16_t &p2p_port,std::string stream_name)
{
    TrackerSession *streamer_session;
    
    _core = core;
    fast_new2(TrackerSession, streamer_session, streamer, p2p_port);
    
    _stream_name = stream_name;
    _streamer = streamer_session;
    
    streamer_session->_stream_position = _clients_by_neighbors_count.Insert(0, streamer_session);    
    _clients_by_solid_session.insert(std::make_pair(streamer, streamer_session));
    
    streamer_session->OnNeighborCountChanged.Attach<Stream>(this, &Stream::OnNeighborCountChanged);
}

Stream::~Stream()
{
    SmartPosition<uint32_t, TrackerSession *>   *pos;
    TrackerSession                              *tracker_session;
        
    for (pos = _clients_by_neighbors_count.First(); pos != NULL; pos = pos->GetNext())
    {
        tracker_session = pos->GetValue();
        tracker_session->_udp_session->Disconnect();
        fast_delete(TrackerSession, tracker_session);
    }
}

TrackerSession* Stream::AddSession(SolidUDPSession *session, uint16_t &p2p_port)
{
    TrackerSession *tracker_session;
    
    fast_new3(TrackerSession, tracker_session, session, p2p_port,_streamer);
    
    tracker_session->_stream_position = _clients_by_neighbors_count.Insert(0, tracker_session);
    _clients_by_solid_session  .insert(std::make_pair(session, tracker_session));

    tracker_session->OnNeighborCountChanged.Attach<Stream>(this, &Stream::OnNeighborCountChanged);
    
    return tracker_session;
}

void Stream::DeleteSession(SolidUDPSession *session)
{
    TrackerSession *tracker_session;
    std::map<SolidUDPSession *,TrackerSession *>::iterator it;
    
    it = _clients_by_solid_session.find(session);
    assert(it != _clients_by_solid_session.end());
    
    tracker_session = (*it).second;
    
    tracker_session->_stream_position->Delete();
    _clients_by_solid_session.erase(tracker_session->_udp_session);
    fast_delete(TrackerSession, tracker_session);
}

TrackerSession* Stream::Find(SolidUDPSession *session)
{    
    std::map<SolidUDPSession *,TrackerSession *>::iterator it;
    
    it = _clients_by_solid_session.find(session);
    
    if (it == _clients_by_solid_session.end())
        return false;
    else
        return (*it).second;    
}

void Stream::OnNeighborCountChanged(TrackerSession *tracker_session, uint32_t new_size)
{
    SmartPosition<uint32_t, TrackerSession *> *pos;
    
    pos = tracker_session->_stream_position;
    
    pos->UpdateKey(new_size);
}
