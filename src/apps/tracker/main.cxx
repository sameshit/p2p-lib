#include "ServerTracker.h"

int main()
{
    CoreObject *_core;
    ServerTracker *tracker;
    
    _core = new CoreObject();

    fast_new1(ServerTracker, tracker, _core);
    
    
    ev_run(_core->GetLoop());    
    
    fast_delete(ServerTracker, tracker);
    delete _core;
    
    return 0;
}