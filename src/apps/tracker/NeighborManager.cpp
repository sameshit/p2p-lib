#include "NeighborManager.h"
#include "../../lib/tracker/TrackerMessages.h"

NeighborManager::NeighborManager(CoreObject *core)
:StreamManager(core)
{
    ev_timer_init(&_neighbor_timer, NeighborManager::NeighborTimerTimeout, 0.25, 0.25);
    
    _neighbor_timer.data = this;
    ev_timer_start(_core->GetLoop(), &_neighbor_timer);
}

NeighborManager::~NeighborManager()
{
    ev_timer_stop(_core->GetLoop(), &_neighbor_timer);
}

void NeighborManager::NeighborTimerTimeout(struct ev_loop *loop, ev_timer *w, int revents)
{
    NeighborManager                             *neighbor_manager = (NeighborManager *)w->data;
    std::map<std::string,Stream *>::iterator    it;
    Stream                                      *stream;
    SmartPosition<uint32_t, TrackerSession *>   *pos1,*pos2;
    TrackerSession                              *tracker_session;
    CoreObject                                  *_core = neighbor_manager->_core;
    uint32_t                                    ip2;
    uint16_t                                    port2;
    uint32_t                                    count;
	uint8_t										data[7];
    
    for (it = neighbor_manager->_streams_by_name.begin(); it != neighbor_manager->_streams_by_name.end(); it ++)
    {
        stream = (*it).second;                    
        for (pos1 = stream->_clients_by_neighbors_count.First(); 
             pos1 != NULL && pos1->GetKey() < NEIGHBORMANAGER_DEFAULTNEIGHBORCOUNT; 
             pos1 = pos1->GetNext())
        {
            count = NEIGHBORMANAGER_DEFAULTNEIGHBORCOUNT - pos1->GetKey();
            for (pos2 = stream->_clients_by_neighbors_count.First(); 
                 pos2 != NULL && count != 0; 
                 pos2 = pos2->GetNext())
            {
                ip2     = pos2->GetValue()->GetUDPSession()->GetNetworkHost();
                port2   = pos2->GetValue()->GetP2PPort();
                
                if (pos1 == pos2 || pos1->GetValue()->NeighborExists(ip2, port2))
                    continue;
                
                tracker_session = pos1->GetValue();
				TrackerMessages::SetNewNeighbor(_core,data,ip2,port2);
                tracker_session->GetUDPSession()->Send(data,7);                
                count --;                
            }
        }
    }
}
