#ifndef AuthOperations__H
#define AuthOperations__H

#include "HiredisConnection.h"
#include "../../lib/protocols/solidudp/SolidUDPProtocol.h"
#include "../../lib/tracker/TrackerMessages.h"
#include <string>

typedef struct TemporaryData
{
    void *ptr1;
    void *ptr2;
    void *ptr3;
    void *ptr4;
}TemporaryData;

class AuthOperations
:public HiredisConnection
{
public:
    AuthOperations(CoreObject *core,const char *hiredis_host="127.0.0.1",uint16_t hiredis_port = 6379);
    ~AuthOperations();
    
    void StreamerAuth(SolidUDPSession *,std::string &stream_name);    
    void DeleteStream(std::string &stream_name);
    
    Event2<SolidUDPSession *,bool &> OnStreamerAuth;     
private:                
    static void StreamerAuthStreamExists        (redisAsyncContext *c, void *preply, void *privdata);
};

#endif
