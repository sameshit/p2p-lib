#ifndef STREAMER__H
#define STREAMER__H

#include "../../lib/tracker/TrackerClient.h"
#include "../../lib/protocols/p2pprotocol/P2PProtocol.h"
#include "ffmpeglistener/FFmpegListener.h"

class Streamer 
{
public:
    Streamer(CoreObject *core,const char *stream_name);
    virtual ~Streamer();
    
    void Stop(SolidUDPSession *session);
private:
    CoreObject          *_core;
    BufferManager       *buffer_manager;
    TrackerClient       *tracker_client;
    P2PProtocol         *p2p_protocol;
    FFmpegListener      *ffmpeg_listener;
};

#endif