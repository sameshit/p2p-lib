#include "FFmpegListener.h"

FFmpegListener::FFmpegListener(CoreObject *core,BufferManager *buffer_manager,uint16_t port)
:TCPListener(core,port)
{
    _buffer_manager = buffer_manager;    
    _last_block_id  = 0;
    _status = FFMPEG_WAIT_FOR_HEADER;
}

FFmpegListener::~FFmpegListener()
{

}

void FFmpegListener::OnNewConnection(TCPConnection *connection)
{
    if (_connections.size() > 1)
    {
        DeleteConnection(connection);
        return;
    }        

    connection->SetMessageMode(TCP_FIXED_MSG_SIZE);    
    connection->SetMessageSize(49);
    connection->StopWrite();
}

void FFmpegListener::OnNewMessage(TCPConnection *connection, uint8_t *data, ssize_t size)
{        
    if (_status == FFMPEG_WAIT_FOR_HEADER)
    {
        assert(size == 49);
        _buffer_manager->AddHeader(data, size);
        
        _status = FFMPEG_WAIT_FOR_PACKET;
        
        connection->SetMessageSize(FFMPEG_BLOCK_SIZE);
        
        LOG<< "Header recvd"<<std::endl;
    }
    else if (_status == FFMPEG_WAIT_FOR_PACKET)
    {
        assert(size == FFMPEG_BLOCK_SIZE);
        _last_block_id ++;
        _buffer_manager->Add(_last_block_id, data, size);
        
//        LOG("Packet recvd with size %d",size);
    }    
    else
        assert(false);
}

void FFmpegListener::OnDeleteConnection(TCPConnection *connection)
{
//    ev_break(_core->GetLoop(),EVBREAK_ONE);
    _status = FFMPEG_WAIT_FOR_HEADER;
}
