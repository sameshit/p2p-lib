#ifndef FFMPEGLISTENER__H
#define FFMPEGLISTENER__H

#include "../../../lib/core/protocols/TCP/TCPListener.h"
#include "../../../lib/buffer/BufferManager.h"

#define FFMPEG_WAIT_FOR_HEADER 1
#define FFMPEG_WAIT_FOR_PACKET 2

#define FFMPEG_BLOCK_SIZE 32768
#define FFMPEG_DEFAULT_PORT 32094

class FFmpegListener 
:public TCPListener
{
public:
    FFmpegListener(CoreObject *core,BufferManager *buffer_manager,uint16_t port=32094);
    virtual ~FFmpegListener();
    
    void OnNewMessage(TCPConnection* connection,uint8_t *data, ssize_t size);
    void OnNewConnection(TCPConnection *connection);
    void OnDeleteConnection(TCPConnection *connection);
    void OnHeader(uint8_t *data,ssize_t size);
    void OnBlock(uint8_t *data,ssize_t size);
private:
    BufferManager *_buffer_manager;
    uint32_t _last_block_id;
    uint8_t _status;
};

#endif
