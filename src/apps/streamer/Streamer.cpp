#include "Streamer.h"

Streamer::Streamer(CoreObject *core,const char* stream_name)
{
	uint8_t *data;
	size_t size;

    _core = core;
    
    fast_new1(BufferManager, buffer_manager, _core);
    fast_new2(FFmpegListener,ffmpeg_listener,_core,buffer_manager);
    fast_new1(P2PProtocol, p2p_protocol, _core);
    fast_new2(TrackerClient, tracker_client, _core, "127.0.0.1");
    
    
    tracker_client->OnSetNeighbors.Attach<P2PProtocol>(p2p_protocol,&P2PProtocol::SetNewNeighbor);
    
    p2p_protocol->OnConnect.   Attach<TrackerClient>(tracker_client, &TrackerClient::NewNeighbor);
    p2p_protocol->OnDisconnect.Attach<TrackerClient>(tracker_client, &TrackerClient::OldNeighbor);

	size = strlen(stream_name) + 3;
	data = fast_alloc(uint8_t,size);
	TrackerMessages::StreamerAuth(_core,data,size,p2p_protocol->GetNetworkPort(),stream_name);
    tracker_client->GetSession()->Send(data,size);
	fast_free(data);
    
    tracker_client->StopSpeedCalcTimer();
    
    tracker_client->OnDisconnect.Attach<Streamer>(this,&Streamer::Stop);
}


Streamer::~Streamer()
{
    fast_delete(TrackerClient   ,tracker_client);
    fast_delete(P2PProtocol     ,p2p_protocol);
    fast_delete(FFmpegListener  ,ffmpeg_listener);
    fast_delete(BufferManager   ,buffer_manager);
}

void Streamer::Stop(SolidUDPSession *session) 
{
    _core->StopLoop();
}
