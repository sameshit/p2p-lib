#include "Streamer.h"


int main ()
{
    CoreObject *_core;
    Streamer   *streamer;
            
    _core = new CoreObject();
    
    fast_new2(Streamer,streamer,_core,"TestStream");
    
    ev_run(_core->GetLoop());
    
    fast_delete(Streamer, streamer);
    delete _core;
    
    return 0;
}