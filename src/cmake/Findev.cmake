SET(CMAKE_FIND_LIBRARY_SUFFIXES .a ${CMAKE_FIND_LIBRARY_SUFFIXES})

find_path(LIBEV_INCLUDE_DIR ev.h
          HINTS /usr/local/include /usr/include/
          PATH_SUFFIXES libev )

find_library(LIBEV_LIBRARY NAMES ev libev
             HINTS /usr/local/lib/ /usr/lib/ )

set(LIBEV_LIBRARIES ${LIBEV_LIBRARY} )
set(LIBEV_INCLUDE_DIRS ${LIBEV_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBEV_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(LibEv  DEFAULT_MSG
                                  LIBEV_LIBRARY LIBEV_INCLUDE_DIR)

mark_as_advanced(LIBEV_INCLUDE_DIR LIBEV_LIBRARY )
