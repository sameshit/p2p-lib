SET(CMAKE_FIND_LIBRARY_SUFFIXES .a ${CMAKE_FIND_LIBRARY_SUFFIXES})

find_path(LIBHIREDIS_INCLUDE_DIR hiredis.h
          HINTS /usr/local/include /usr/include/
          PATH_SUFFIXES hiredis )

find_library(LIBHIREDIS_LIBRARY NAMES hiredis libhiredis
             HINTS /usr/local/lib/ /usr/lib/ )

set(LIBHIREDIS_LIBRARIES ${LIBHIREDIS_LIBRARY} )
set(LIBHIREDIS_INCLUDE_DIRS ${LIBHIREDIS_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBEV_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(LibHiredis  DEFAULT_MSG
                                  LIBHIREDIS_LIBRARY LIBHIREDIS_INCLUDE_DIR)

mark_as_advanced(LIBHIREDIS_INCLUDE_DIR LIBHIREDIS_LIBRARY )
