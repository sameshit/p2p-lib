SET(CMAKE_FIND_LIBRARY_SUFFIXES .a ${CMAKE_FIND_LIBRARY_SUFFIXES})

find_path(LIBUDT4_INCLUDE_DIR udt.h
          HINTS /usr/local/include /usr/include/
          PATH_SUFFIXES udt4)

find_library(LIBUDT4_LIBRARY NAMES udt libudt
             HINTS /usr/local/lib/ /usr/lib/ )

set(LIBUDT4_LIBRARIES ${LIBUDT4_LIBRARY} )
set(LIBUDT4_INCLUDE_DIRS ${LIBUDT4_INCLUDE_DIR} )

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set LIBEV_FOUND to TRUE
# if all listed variables are TRUE
find_package_handle_standard_args(LibUDT4  DEFAULT_MSG
                                  LIBUDT4_LIBRARY LIBUDT4_INCLUDE_DIR)

mark_as_advanced(LIBUDT4_INCLUDE_DIR LIBUDT4_LIBRARY )
