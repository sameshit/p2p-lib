#ifndef TRACKERMESSAGES__H
#define TRACKERMESSAGES__H

#include "../protocols/solidudp/SolidUDPMessage.h"

#define TRACKER_STREAMERAUTH            1
#define TRACKER_STREAMERAUTHOK          2
#define TRACKER_STREAMERAUTHDENIED      3
#define TRACKER_CLIENTAUTH              4
#define TRACKER_CLIENTAUTHNOSUCHSTREAM  5
#define TRACKER_CLIENTAUTHOK            6
#define TRACKER_SETNEWNEIGHBOR          7
#define TRACKER_NEWNEIGHBOR             8
#define TRACKER_OLDNEIGHBOR             9

#define TRACKERDEFAULTPORT 15472

class TrackerMessages 
{
public:
    static void StreamerAuth            (CoreObject *_core,uint8_t *data,size_t size,uint16_t p2p_port,const char *stream_name);
    static void StreamerAuthOk          (CoreObject *_core,uint8_t *data);
    static void StreamerAuthDenied      (CoreObject *_core,uint8_t *data);
    static void ClientAuth              (CoreObject *_core,uint8_t *data,size_t size,uint16_t p2p_port,const char* stream_name);
    static void ClientAuthNoSuchStream  (CoreObject *_core,uint8_t *data);
    static void ClientAuthOk            (CoreObject *_core,uint8_t *data);
    static void SetNewNeighbor          (CoreObject *_core,uint8_t *data,uint32_t ip,uint16_t port);
    static void NewNeighbor             (CoreObject *_core,uint8_t *data,uint32_t ip,uint16_t port);
    static void OldNeighbor             (CoreObject *_core,uint8_t *data,uint32_t ip,uint16_t port);
};

#endif
