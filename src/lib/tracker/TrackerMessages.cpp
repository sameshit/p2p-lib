#include "TrackerMessages.h"

void TrackerMessages::StreamerAuth(CoreObject *_core,uint8_t *data,size_t size,uint16_t p2p_port,const char *stream_name)
{
    data[0] = TRACKER_STREAMERAUTH;    
    Utils::PutBe16(&data[1],p2p_port);    

    memcpy(&data[3],stream_name,size - 3);
}

void TrackerMessages::StreamerAuthOk(CoreObject *_core,uint8_t *data)
{
    data[0] = TRACKER_STREAMERAUTHOK;    
}

void TrackerMessages::StreamerAuthDenied(CoreObject *_core,uint8_t *data)
{
    data[0] = TRACKER_STREAMERAUTHDENIED;
}

void TrackerMessages::ClientAuth(CoreObject *_core,uint8_t *data,size_t size,uint16_t p2p_port, const char *stream_name)
{
    data[0] = TRACKER_CLIENTAUTH;
    Utils::PutBe16(&data[1],p2p_port);
    memcpy(&data[3],stream_name,size - 3);
}

void TrackerMessages::ClientAuthNoSuchStream(CoreObject *_core,uint8_t *data)
{
    data[0] = TRACKER_CLIENTAUTHNOSUCHSTREAM;
}

void TrackerMessages::ClientAuthOk(CoreObject *_core,uint8_t *data)
{
    data[0] = TRACKER_CLIENTAUTHOK;
}

void TrackerMessages::SetNewNeighbor(CoreObject *_core,uint8_t *data,uint32_t ip,uint16_t port)
{
    data[0] = TRACKER_SETNEWNEIGHBOR;
    
    Utils::PutBe32(&data[1], ip);
    Utils::PutBe16(&data[5], port);
}

void TrackerMessages::NewNeighbor(CoreObject *_core,uint8_t *data,uint32_t ip,uint16_t port)
{
    data[0] = TRACKER_NEWNEIGHBOR;
    Utils::PutBe32(&data[1], ip);
    Utils::PutBe16(&data[5], port);
}

void TrackerMessages::OldNeighbor(CoreObject *_core,uint8_t *data,uint32_t ip, uint16_t port)
{
    data[0] = TRACKER_OLDNEIGHBOR;
    Utils::PutBe32(&data[1], ip);
    Utils::PutBe16(&data[5], port);    
};
