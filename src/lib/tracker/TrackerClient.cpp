#include "TrackerClient.h"

TrackerClient::TrackerClient(CoreObject *core,const char *tracker_hostname,uint16_t port)
:SolidUDPProtocol(core,rand() % UINT16_MAX)
{
    _session = CreateSession(inet_addr(tracker_hostname), htons(port));

	NewMessageEvt.Attach<TrackerClient>(this,&TrackerClient::OnNewMessage);
}

TrackerClient::~TrackerClient()
{

}

void TrackerClient::OnNewMessage(SolidUDPSession *from, uint8_t *data, ssize_t size)
{
    uint32_t        host;
    uint16_t        port;
    
    if (size < 1)
    {
        LOG << "Invalid message from " <<from->GetRealHost()<<":"<< from->GetRealPort() << std::endl;
        return;
    }
    
    switch (data[0])
    {
        case TRACKER_CLIENTAUTHOK:
            LOG << "Successfully authenticated on tracker" << std::endl;
        break;
        case TRACKER_STREAMERAUTHOK:
            LOG << "Started stream on tracker" << std::endl;
        break;
        case TRACKER_CLIENTAUTHNOSUCHSTREAM:
            LOG << "No such stream" << std::endl;
        break;
        case TRACKER_STREAMERAUTHDENIED:
            LOG << "Unable to start stream on tracker" << std::endl;
        break;
        case TRACKER_SETNEWNEIGHBOR:            
            if (size != 7)
            {
                LOG << "Invalid set neighbor message from "<<from->GetRealHost()<<":"<<from->GetRealPort() << std::endl;
                return;
            }
            
            host = Utils::GetBe32(&data[1]);
            port = Utils::GetBe16(&data[5]);
            
            LOG << "Set new neighbor("<<Utils::GetRealHost(host)<<":"<<Utils::GetRealPort(port)<<") request"<< std::endl;
            
            OnSetNeighbors(host,port);            
        break;
    }
}

void TrackerClient::NewNeighbor(SolidUDPSession *p2p_neighbor)
{
	uint8_t data[7];

	TrackerMessages::NewNeighbor(_core,data,p2p_neighbor->GetNetworkHost(),p2p_neighbor->GetNetworkPort());
	_session->Send(data,7);

    LOG << "Sending NewNeighbor to tracker ("<<p2p_neighbor->GetNetworkHost()<<":"<<p2p_neighbor->GetNetworkPort()<<")"<<std::endl;
}

void TrackerClient::OldNeighbor(SolidUDPSession *p2p_neighbor)
{
	uint8_t data[7];

	TrackerMessages::OldNeighbor(_core,data,p2p_neighbor->GetNetworkHost(), p2p_neighbor->GetNetworkPort() );
	_session->Send(data,7);

    LOG << "Sending OldNeighbor to tracker ("<<p2p_neighbor->GetNetworkHost()<<":"<<p2p_neighbor->GetNetworkPort()<<")"<<std::endl;
}
