#ifndef TrackerClient__H
#define TrackerClient__H

#include "../protocols/solidudp/SolidUDPProtocol.h"
#include "TrackerMessages.h"


class TrackerClient
:public SolidUDPProtocol
{
public:
    TrackerClient(CoreObject *core,const char *tracker_hostname, uint16_t tracker_port=TRACKERDEFAULTPORT);
    virtual ~TrackerClient();    
    
    SolidUDPSession *GetSession() {return _session;}
    
    void OnNewMessage(SolidUDPSession *from,uint8_t *data,ssize_t size);
    
    void NewNeighbor(SolidUDPSession *p2p_session);
    void OldNeighbor(SolidUDPSession *p2p_session);
        
    Event2<uint32_t, uint16_t> OnSetNeighbors;
private:
    SolidUDPSession *_session;    
};

#endif
