#ifndef LOGGEREVENTHANDLER_H
#define LOGGEREVENTHANDLER_H

#include "../../common/config.h"

#define ENABLE_LOGGER
#ifdef ENABLE_LOGGER
    #define LOG _core->GetLogger()->LogIt(__FILE__,__FUNCTION__,__LINE__)
    #define LONG_LOG(logger) logger->LogIt(__FILE__,__FUNCTION__,__LINE__)
#else
    #define LOG
    #define LONG_LOG(logger)
#endif

class Logger
{
public:
	Logger();
	Logger(const char* filename);	
	virtual ~Logger();

	std::ostream& LogIt(const char* file, const char* function, int line);
private:
	bool 			_isfile;
	std::ostream 	*out_stream;
};

#endif
