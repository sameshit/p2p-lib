#include "Logger.h"
using namespace std;

Logger::Logger()
:_isfile(false)
{
	out_stream = &cout;
}

Logger::Logger(const char *filename)
:_isfile(true)
{
	out_stream = new ofstream(filename);
}

Logger::~Logger()
{
	if (_isfile)
		delete out_stream;
}


ostream& Logger::LogIt(const char *file, const char* function, int line)
{
	char prefix_format[20] = "";
	time_t now;
	struct tm* timeinfo;
    
    time(&now);
    timeinfo = localtime(&now);
    
	strftime(prefix_format,20,"(%I:%M:%S):",timeinfo);	
	
	(*out_stream) << prefix_format << file << "(" << function << "," << line << "):";

	return (*out_stream);
}
