#include "CoreObject.h"


CoreObject::CoreObject()
{
	_loop 	= ev_loop_new(0);
	_logger = new Logger();
   	_memory	= new Memory(_logger);

    srand ( time(NULL) );
    
    _int_sig.data = this;
    ev_signal_init (&_int_sig, CoreObject::InterruptCallback, SIGINT);
    ev_signal_start (_loop, &_int_sig);
}

CoreObject::CoreObject(const char *logger_filename)
{
	_loop 	= ev_loop_new(0);
  	_logger = new Logger(logger_filename);
   	_memory	= new Memory(_logger);
}

CoreObject::~CoreObject()
{
  	delete _memory;
	delete _logger;
	ev_loop_destroy(_loop);	
}

void CoreObject::InterruptCallback(struct ev_loop *loop, ev_signal *w, int revents)
{
    CoreObject *_core = (CoreObject*)w->data;
    
    LOG <<"Interrupt signal received. Stopping event loop and exiting app" << std::endl;
    ev_break (loop);
}
