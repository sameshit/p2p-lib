#ifndef COREOBJECT__H
#define COREOBJECT__H

#include "../common/config.h"
#include "memory/Memory.h"
#include "logger/Logger.h"
#include "event/Event.h"
#include "thread/Thread.h"
#include "containers/SmartSortedList.h"

class CoreObject
{
public:
	CoreObject();
    CoreObject(const char* logger_filename);
	virtual ~CoreObject();
	inline Memory* 		GetMemory() 	{return _memory;}
	inline struct ev_loop* 	GetLoop()	{return _loop;}
	Logger*             GetLogger()	{return _logger;}
    inline void StopLoop()          {ev_break(_loop);}
    inline void StartLoop()         {ev_run(_loop);}
protected:
	struct ev_loop 		*_loop;
	Memory              *_memory;
	Logger              *_logger;
    ev_signal           _int_sig;
    static void  InterruptCallback(struct ev_loop *loop, ev_signal *w, int revents);
};

#endif
