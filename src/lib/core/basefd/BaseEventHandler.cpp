#include "BaseEventHandler.h"

BaseEventHandler::BaseEventHandler(struct ev_loop *loop,int fd)
{
	_loop = loop;
	_fd   = fd;

	w_io.data = this;
	r_io.data = this;
	ev_io_init (&w_io, BaseEventHandler::StaticOnWrite, _fd, EV_WRITE);
	ev_io_init (&r_io, BaseEventHandler::StaticOnRead,  _fd, EV_READ);
	ev_io_start (loop, &r_io);
    
    _already_read   = true;
    _already_write  = false;
}

BaseEventHandler::~BaseEventHandler()
{
	ev_io_stop(_loop,&r_io);
	ev_io_stop(_loop,&w_io);
}

void BaseEventHandler::StaticOnRead(struct ev_loop *loop, ev_io *w, int events)
{
	BaseEventHandler *carrier = (BaseEventHandler *)w->data;	

	carrier->OnRead();
}

void BaseEventHandler::StaticOnWrite(struct ev_loop *loop, ev_io *w, int events)
{
	BaseEventHandler *carrier = (BaseEventHandler *)w->data;

	carrier->OnWrite();
}

void BaseEventHandler::OnRead()
{
	fprintf(stderr,"read event\n");
}

void BaseEventHandler::OnWrite()
{
	fprintf(stderr,"write event\n");
}