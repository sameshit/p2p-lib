#ifndef BASEEVENTHANDLER_H
#define BASEEVENTHANDLER_H
#include "../../common/config.h"

class BaseEventHandler
{
public:
	BaseEventHandler(struct ev_loop *loop,int fd);
	virtual ~BaseEventHandler();
	virtual void OnRead();
	virtual void OnWrite();
    
    inline void StopRead() {_already_read = false;ev_io_stop(_loop, &r_io);}
    inline void StopWrite() {_already_write = false;ev_io_stop(_loop,&w_io);}
    
    inline void StartRead() {if (!_already_read) ev_io_start(_loop, &r_io);}
    inline void StartWrite() { if (!_already_write) ev_io_start(_loop, &w_io);}
private:
	struct ev_loop 		*_loop;
	ev_io 			w_io;	
	ev_io			r_io;
    bool _already_read;
    bool _already_write;
	static void StaticOnRead(struct ev_loop *loop, ev_io *w, int events);
	static void StaticOnWrite(struct ev_loop *loop, ev_io *w, int events);
protected:
	int _fd;
};

#endif
