#ifndef SMARTLIST__H
#define SMARTLIST__H

#include "../event/Event.h"
#include "../../common/config.h"

template <typename Key,typename Value> 
class SmartPosition
{
public:
    typedef Event1<SmartPosition<Key,Value> *> _Event1;
    typedef SmartPosition<Key,Value>           _SmartPosition;
    
    SmartPosition(Key key,Value value)
    {
        _key        = key;
        _value      = value;
    }
    ~SmartPosition()
    {
    
    }
    void UpdateKey(Key new_key)
    {
        _key = new_key;
        OnUpdateKey(this);
    }
    void UpdateValue(Value new_value)
    {
        _value = new_value;
    }
    void Delete()
    {
        OnDelete(this);
        
        delete this;
    }
    Value GetValue()
    {
        return _value;
    }
    Key GetKey()
    {
        return _key;
    }
    _SmartPosition* GetNext()
    {
        return _next;
    }
    _SmartPosition* GetPrev()
    {
        return _prev;
    }
private:
    _SmartPosition               *_next;
    _SmartPosition               *_prev;
    _Event1                     OnDelete;
    _Event1                     OnUpdateKey;
    Key                         _key;
    Value                       _value;    
    
    template<class,class> friend class SmartSortedList;
};


template <typename Key,typename Value>
class SmartSortedList
{
public:
    typedef SmartPosition<Key,Value>           _SmartPosition;
    typedef Event1<_SmartPosition *>           _Event1;  
    typedef SmartSortedList<Key,Value>         _SmartSortedList;
    SmartSortedList() // O(1)
    {
        first = NULL;
        _size = 0;
    }
    ~SmartSortedList() // O(N)
    {
        _SmartPosition *tmp,*prev = NULL;
        
        for (tmp = first; tmp != NULL; prev = tmp, tmp = tmp->GetNext())
            if (prev != NULL)
                delete prev;
        
        if (prev != NULL)
            delete prev;
            
    }
    
    _SmartPosition* Insert(Key key,Value value) // O(N)
    {
        _SmartPosition *new_pos = new _SmartPosition(key,value);
        
        new_pos->OnDelete.   template Attach<_SmartSortedList >(this,&SmartSortedList::OnDelete);
        new_pos->OnUpdateKey.template Attach<_SmartSortedList >(this,&SmartSortedList::OnUpdateKey);
        
        _size ++;
        
        if (first != NULL)
            first->_prev = new_pos;

        new_pos->_next = first;
        new_pos->_prev = NULL;
        
        first = new_pos;
        
        new_pos->UpdateKey(key);
        return new_pos;
    }    
    
    uint64_t GetSize()
    {
        return _size;
    }
    
    _SmartPosition* First()
    {
        return first;
    }
private:    
    _SmartPosition *first;
    uint64_t       _size;
    
    void OnDelete(_SmartPosition *pos) // O(1)
    {
        _SmartPosition *prev,*next;
        
        prev = pos->_prev;
        next = pos->_next;
        
        if (prev == NULL)
            first = next;
        else
        {
            prev->_next = next;
            if (next != NULL)
                next->_prev = prev;
        }
        
        _size --;
    }
    
    void OnUpdateKey(_SmartPosition *pos) // O(c)
    {
        _SmartPosition *tmp;                
                
        if (pos->_prev == NULL || pos->_prev->GetKey() < pos->GetKey() )
        {
            for (tmp = pos->_next; tmp != NULL && tmp->GetKey() < pos->GetKey(); tmp = pos->_next)
                SwapWithNext(pos);
        }
        else
        {
            for (tmp = pos->_prev; tmp != NULL && tmp->GetKey() > pos->GetKey(); tmp = pos->_prev)
                SwapWithNext(tmp);
        }
                
    }   
    
    void SwapWithNext(_SmartPosition *pos) // O(1)
    {
        _SmartPosition *left,*right, *pos_next;
        
        left        = pos->_prev;
        right       = pos->_next->_next;
        pos_next    = pos->_next; 
                            
        if (left != NULL)
            left->_next  = pos_next;
        else
            first = pos_next;

        if (right != NULL)
            right->_prev = pos;
        
        pos_next->_next = pos;
        pos_next->_prev = left;
        
        pos     ->_next = right;
        pos     ->_prev = pos_next;        
    }
};

#endif