#ifndef EVENT___H
#define EVENT___H

// event handlers base

class EventHandlerBase0
{
public:
    virtual void notify() = 0;
};

template <typename Arg1>
class EventHandlerBase1
{
public:
    virtual void notify(Arg1 arg1) = 0;
};

template <typename Arg1,typename Arg2>
class EventHandlerBase2
{
public:
    virtual void notify(Arg1 arg1,Arg2 arg2) = 0;
};

template <typename Arg1,typename Arg2,typename Arg3>
class EventHandlerBase3
{
public:
    virtual void notify(Arg1 arg1,Arg2 arg2,Arg3 arg3) = 0;
};

template <typename Arg1,typename Arg2,typename Arg3, typename Arg4>
class EventHandlerBase4
{
public:
    virtual void notify(Arg1 arg1,Arg2 arg2,Arg3 arg3, Arg4 arg4) = 0;
};

template <typename Arg1,typename Arg2,typename Arg3, typename Arg4,typename Arg5>
class EventHandlerBase5
{
public:
    virtual void notify(Arg1 arg1,Arg2 arg2,Arg3 arg3, Arg4 arg4,Arg5 arg5) = 0;
};

// event handlers 


template <typename ListenerT>
class EventHandler0 : public EventHandlerBase0
{
    typedef void (ListenerT::*PtrMember)();
    ListenerT* m_object;
    PtrMember m_member;
    
public:
    
    EventHandler0(ListenerT* object, PtrMember member)
    : m_object(object), m_member(member)
    {}
    
    void notify()
    {
        (m_object->*m_member)();
    }
};

template <typename ListenerT,typename Arg1>
class EventHandler1 : public EventHandlerBase1<Arg1>
{
    typedef void (ListenerT::*PtrMember)(Arg1);
    ListenerT* m_object;
    PtrMember m_member;
    
public:
    
    EventHandler1(ListenerT* object, PtrMember member)
    : m_object(object), m_member(member)
    {}
    
    void notify(Arg1 arg1)
    {
        (m_object->*m_member)(arg1);
    }
};

template <typename ListenerT,typename Arg1, typename Arg2>
class EventHandler2 : public EventHandlerBase2<Arg1,Arg2>
{
    typedef void (ListenerT::*PtrMember)(Arg1,Arg2);
    ListenerT* m_object;
    PtrMember m_member;
    
public:
    
    EventHandler2(ListenerT* object, PtrMember member)
    : m_object(object), m_member(member)
    {}
    
    void notify(Arg1 arg1,Arg2 arg2)
    {
        (m_object->*m_member)(arg1,arg2);
    }
};

template <typename ListenerT,typename Arg1, typename Arg2, typename Arg3>
class EventHandler3 : public EventHandlerBase3<Arg1,Arg2,Arg3>
{
    typedef void (ListenerT::*PtrMember)(Arg1,Arg2,Arg3);
    ListenerT* m_object;
    PtrMember m_member;
    
public:
    
    EventHandler3(ListenerT* object, PtrMember member)
    : m_object(object), m_member(member)
    {}
    
    void notify(Arg1 arg1,Arg2 arg2,Arg3 arg3)
    {
        (m_object->*m_member)(arg1,arg2,arg3);
    }
};

template <typename ListenerT,typename Arg1, typename Arg2, typename Arg3,typename Arg4>
class EventHandler4 : public EventHandlerBase4<Arg1,Arg2,Arg3,Arg4>
{
    typedef void (ListenerT::*PtrMember)(Arg1,Arg2,Arg3,Arg4);
    ListenerT* m_object;
    PtrMember m_member;
    
public:
    
    EventHandler4(ListenerT* object, PtrMember member)
    : m_object(object), m_member(member)
    {}
    
    void notify(Arg1 arg1,Arg2 arg2,Arg3 arg3,Arg4 arg4)
    {
        (m_object->*m_member)(arg1,arg2,arg3,arg4);
    }
};

template <typename ListenerT,typename Arg1, typename Arg2, typename Arg3,typename Arg4,typename Arg5>
class EventHandler5 : public EventHandlerBase5<Arg1,Arg2,Arg3,Arg4,Arg5>
{
    typedef void (ListenerT::*PtrMember)(Arg1,Arg2,Arg3,Arg4,Arg5);
    ListenerT* m_object;
    PtrMember m_member;
    
public:
    
    EventHandler5(ListenerT* object, PtrMember member)
    : m_object(object), m_member(member)
    {}
    
    void notify(Arg1 arg1,Arg2 arg2,Arg3 arg3,Arg4 arg4,Arg5 arg5)
    {
        (m_object->*m_member)(arg1,arg2,arg3,arg4,arg5);
    }
};

// Events 
class Event0
{
    typedef std::map<int,EventHandlerBase0 *> HandlersMap;
    typedef HandlersMap::iterator HandlersIterator;
    HandlersMap m_handlers;
    int m_count;
    
public:
    
    
    Event0()
    : m_count(0) {}
    
    ~Event0()
    {
        HandlersIterator it;
        for(it = m_handlers.begin(); it != m_handlers.end(); it++)
            delete it->second;
        m_handlers.clear();
    }
    
    template <typename ListenerT>
    void Attach(ListenerT* object,void (ListenerT::*member)())
    {
        typedef void (ListenerT::*PtrMember)();
        m_handlers[m_count] = new EventHandler0<ListenerT>(object,member);
        m_count++;
    }
    
    bool Deattach(int id)
    {
        HandlersIterator it = m_handlers.find(id);
        
        if(it == m_handlers.end())
            return false;
        
        delete it->second;
        m_handlers.erase(it);
        m_count--;        
        return true;
    }
    
    void operator ()()
    {
        HandlersIterator it;
        for(it = m_handlers.begin(); it != m_handlers.end(); it++)
            it->second->notify();  
    }
};

template <typename Arg1>
class Event1
{
    typedef std::map<int,EventHandlerBase1<Arg1> *> HandlersMap;
    typedef typename HandlersMap::iterator HandlersIterator;
    HandlersMap m_handlers;
    int m_count;
    
public:
    
    
    Event1()
    : m_count(0) {}
    
    ~Event1()
    {
        HandlersIterator it;
        for(it = m_handlers.begin(); it != m_handlers.end(); it++)
            delete it->second;
        m_handlers.clear();
    }
    
    template <typename ListenerT>
    void Attach(ListenerT* object,void (ListenerT::*member)(Arg1))
    {
        typedef void (ListenerT::*PtrMember)(Arg1);
        m_handlers[m_count] = (new EventHandler1<ListenerT,
                               Arg1>(object,member));
        m_count++;
    }
    
    bool Deattach(int id)
    {
        HandlersIterator it = m_handlers.find(id);
        
        if(it == m_handlers.end())
            return false;
        
        delete it->second;
        m_handlers.erase(it);
        m_count--;
        return true;
    }
    
    void operator ()(Arg1 arg1)
    {
        HandlersIterator it;
        for(it = m_handlers.begin(); it != m_handlers.end(); it++)
            it->second->notify(arg1);  
    }
};

template <typename Arg1,typename Arg2>
class Event2
{
    typedef std::map<int,EventHandlerBase2<Arg1,Arg2> *> HandlersMap;
    typedef typename HandlersMap::iterator HandlersIterator;
    HandlersMap m_handlers;
    int m_count;
    
public:
    
    
    Event2()
    : m_count(0) {}
    
    ~Event2()
    {
        HandlersIterator it;
        for(it = m_handlers.begin(); it != m_handlers.end(); it++)
            delete it->second;
        m_handlers.clear();
    }
    
    template <typename ListenerT>
    void Attach(ListenerT* object,void (ListenerT::*member)(Arg1,Arg2))
    {
        typedef void (ListenerT::*PtrMember)(Arg1,Arg2);
        m_handlers[m_count] = (new EventHandler2<ListenerT,
                               Arg1,Arg2>(object,member));
        m_count++;
    }
    
    bool Deattach(int id)
    {
        HandlersIterator it = m_handlers.find(id);
        
        if(it == m_handlers.end())
            return false;
        
        delete it->second;
        m_handlers.erase(it);
        m_count--;        
        return true;
    }
    
    void operator ()(Arg1 arg1,Arg2 arg2)
    {
        HandlersIterator it;
        for(it = m_handlers.begin(); it != m_handlers.end(); it++)
            it->second->notify(arg1,arg2);  
    }
};

template <typename Arg1,typename Arg2, typename Arg3>
class Event3
{
    typedef std::map<int,EventHandlerBase3<Arg1,Arg2,Arg3> *> HandlersMap;
    typedef typename HandlersMap::iterator HandlersIterator;
    HandlersMap m_handlers;
    int m_count;
    
public:
    
    
    Event3()
    : m_count(0) {}
    
    ~Event3()
    {
        HandlersIterator it;
        for(it = m_handlers.begin(); it != m_handlers.end(); it++)
            delete it->second;
        m_handlers.clear();
    }
    
    template <typename ListenerT>
    void Attach(ListenerT* object,void (ListenerT::*member)(Arg1,Arg2,Arg3))
    {
        typedef void (ListenerT::*PtrMember)(Arg1,Arg2,Arg3);
        m_handlers[m_count] = (new EventHandler3<ListenerT,
                               Arg1,Arg2,Arg3>(object,member));
        m_count++;
    }
    
    bool Deattach(int id)
    {
        HandlersIterator it = m_handlers.find(id);
        
        if(it == m_handlers.end())
            return false;
        
        delete it->second;
        m_handlers.erase(it);
        m_count--;
        return true;
    }
    
    void operator ()(Arg1 arg1,Arg2 arg2,Arg3 arg3)
    {
        HandlersIterator it;
        for(it = m_handlers.begin(); it != m_handlers.end(); it++)
            it->second->notify(arg1,arg2,arg3);  
    }
};

template <typename Arg1,typename Arg2, typename Arg3,typename Arg4>
class Event4
{
    typedef std::map<int,EventHandlerBase4<Arg1,Arg2,Arg3,Arg4> *> HandlersMap;
    typedef typename HandlersMap::iterator HandlersIterator;
    HandlersMap m_handlers;
    int m_count;
    
public:
    
    
    Event4()
    : m_count(0) {}
    
    ~Event4()
    {
        HandlersIterator it;
        for(it = m_handlers.begin(); it != m_handlers.end(); it++)
            delete it->second;
        m_handlers.clear();
    }
    
    template <typename ListenerT>
    void Attach(ListenerT* object,void (ListenerT::*member)(Arg1,Arg2,Arg3,Arg4))
    {
        typedef void (ListenerT::*PtrMember)(Arg1,Arg2,Arg3,Arg4);
        m_handlers[m_count] = (new EventHandler4<ListenerT,
                               Arg1,Arg2,Arg3,Arg4>(object,member));
        m_count++;
    }
    
    bool Deattach(int id)
    {
        HandlersIterator it = m_handlers.find(id);
        
        if(it == m_handlers.end())
            return false;
        
        delete it->second;
        m_handlers.erase(it);
        m_count--;        
        return true;
    }
    
    void operator ()(Arg1 arg1,Arg2 arg2,Arg3 arg3,Arg4 arg4)
    {
        HandlersIterator it;
        for(it = m_handlers.begin(); it != m_handlers.end(); it++)
            it->second->notify(arg1,arg2,arg3,arg4);  
    }
};


template <typename Arg1,typename Arg2, typename Arg3,typename Arg4,typename Arg5>
class Event5
{
    typedef std::map<int,EventHandlerBase5<Arg1,Arg2,Arg3,Arg4,Arg5> *> HandlersMap;
    typedef typename HandlersMap::iterator HandlersIterator;
    HandlersMap m_handlers;
    int m_count;
    
public:
    
    
    Event5()
    : m_count(0) {}
    
    ~Event5()
    {
        HandlersIterator it;
        for(it = m_handlers.begin(); it != m_handlers.end(); it++)
            delete it->second;
        m_handlers.clear();
    }
    
    template <typename ListenerT>
    void Attach(ListenerT* object,void (ListenerT::*member)(Arg1,Arg2,Arg3,Arg4,Arg5))
    {
        typedef void (ListenerT::*PtrMember)(Arg1,Arg2,Arg3,Arg4,Arg5);
        m_handlers[m_count] = (new EventHandler5<ListenerT,
                               Arg1,Arg2,Arg3,Arg4,Arg5>(object,member));
        m_count++;
    }
    
    bool Deattach(int id)
    {
        HandlersIterator it = m_handlers.find(id);
        
        if(it == m_handlers.end())
            return false;
        
        delete it->second;
        m_handlers.erase(it);
        m_count--;        
        return true;
    }
    
    void operator ()(Arg1 arg1,Arg2 arg2,Arg3 arg3,Arg4 arg4,Arg5 arg5)
    {
        HandlersIterator it;
        for(it = m_handlers.begin(); it != m_handlers.end(); it++)
            it->second->notify(arg1,arg2,arg3,arg4,arg5);  
    }
};
#endif
