#include "Memory.h"

Memory::Memory(Logger *logger)
{
    _logger = logger;
}

Memory::~Memory()
{
    std::map<void *,PointerInfo *>::iterator alloced_it;
    std::map<size_t,PointerInfo *>::iterator freed_it;
    PointerInfo *pointer_info;
    
#ifdef MEMORY_DEBUG    
    if (_alloced_blocks.size() != _freed_blocks.size())
        LONG_LOG(_logger) << "Found memory leaks. Totally alloced ptrs count is "<<_alloced_blocks.size() <<", but freed only is" << _freed_blocks.size() << std::endl;
    else
        LONG_LOG(_logger) << "There is no memory leaks in this run using fast_alloc\\fast_new" << std::endl;
#endif
    
    for (alloced_it = _alloced_blocks.begin(); alloced_it != _alloced_blocks.end(); alloced_it ++)
    {
        pointer_info = (*alloced_it).second;
              
#ifdef MEMORY_DEBUG        
        if (pointer_info->IsFreed() == false)
        {
            LONG_LOG(_logger) << "Memory leak in file: "<<pointer_info->GetFilename()<<"\nFunction(line): "<< pointer_info->GetFunction() <<"("<<pointer_info->GetLine() <<")\nTime: "<<pointer_info->GetTime() << std::endl;
        
        }
#endif
        delete pointer_info;
    }
 
    
    _freed_blocks.clear();
    _alloced_blocks.clear();
}

#ifdef MEMORY_DEBUG
    void*   Memory::Alloc(size_t size,const char* filename,const char* function,int line)
#else
    void*   Memory::Alloc(size_t size)
#endif
{
    std::map<size_t,PointerInfo *>::iterator freed_it; 
    PointerInfo *pointer_info;

    
    freed_it = _freed_blocks.find(size);
    if (freed_it != _freed_blocks.end())
    {
        pointer_info = (*freed_it).second;
        _freed_blocks.erase(freed_it);        
        pointer_info->SetFreed(false);
    }
    else
    {
        pointer_info = new PointerInfo(size);
        _alloced_blocks.insert(std::make_pair(pointer_info->GetPtr(),pointer_info));
    }
                    
#ifdef MEMORY_DEBUG
    time_t now;
    struct tm* timeinfo;
    char time_str[10];
        
    time(&now);
    timeinfo = localtime(&now);
    
	strftime(time_str,10,"%I:%M:%S",timeinfo);	        

    pointer_info->SetDebugInfo(filename,function,line,time_str);
#endif
            
    return pointer_info->GetPtr();
}

#ifdef MEMORY_DEBUG
    void    Memory::Free(void* ptr,const char* filename,const char* function,int line)
#else
    void    Memory::Free(void* ptr)
#endif
{
    std::map<void *,PointerInfo *>::iterator alloced_it;
    PointerInfo *pointer_info;
    
    alloced_it = _alloced_blocks.find(ptr);
    if (alloced_it == _alloced_blocks.end())
    {
#ifdef MEMORY_DEBUG
        LONG_LOG(_logger) << "Trying to free unexisting pointer.\nFile: "<<filename<<"\nFunction(line): "<<function<<"("<<line<<")" << std::endl;
#else
        LONG_LOG(_logger) << "Trying to free unexisting pointer." << std::endl;
#endif
        
        assert(false);
    }
    
    pointer_info = (*alloced_it).second;
    if (pointer_info->IsFreed())
    {
#ifdef MEMORY_DEBUG        
        LONG_LOG(_logger) << "Trying to free already freed pointer.\nFile: "<<filename<<"\nFunction(line): "<<function<<"("<< line<<")\n" 
        "Previous file: "<<pointer_info->GetFilename()<<"\nPrevious function(line): "<<pointer_info->GetFunction() <<"("<< pointer_info->GetLine() <<")\nTime: "<<pointer_info->GetTime() << std::endl;                
#else
        LONG_LOG(_logger) << "Trying to free already freed pointer." << std::endl;
#endif
        exit(1);
    }
    
    pointer_info->SetFreed(true);

#ifdef MEMORY_DEBUG
    time_t now;
    struct tm* timeinfo;
    char time_str[10];
        
    time(&now);
    timeinfo = localtime(&now);
    
	strftime(time_str,10,"%I:%M:%S",timeinfo);	    
    pointer_info->SetDebugInfo(filename,function,line,time_str);
#endif
    
    _freed_blocks.insert(std::make_pair(pointer_info->GetSize(),pointer_info));    
}