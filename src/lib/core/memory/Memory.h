#ifndef MEMORY___H
#define MEMORY___H

#include "../logger/Logger.h"
#include "PointerInfo.h"
#include <map>
#include <string>

//#undef MEMORY_DEBUG

class Memory
{
public:
    Memory(Logger *logger);
    virtual ~Memory();

#ifdef MEMORY_DEBUG
    void*       Alloc(size_t size,const char* filename,const char* function,int line);
    void        Free(void* ptr,const char* filename,const char* function,int line);
#else
    void*       Alloc(size_t size);
    void        Free(void * ptr);
#endif
private:
    Logger *_logger;
    std::map<void*,PointerInfo*> _alloced_blocks;
    std::multimap<size_t,PointerInfo*> _freed_blocks;
};
//#define MEMORY_STANDART

#ifdef MEMORY_STANDART
#define fast_alloc(type,count) (type*)malloc(sizeof(type)*count)
#define fast_free(ptr)  free(ptr)
#define long_alloc(mem,type,count) fast_alloc(type,count)
#define long_free(mem,ptr) fast_free(ptr)

#define fast_new(type,ptr) ptr = new type
#define fast_new1(type,ptr,arg1) ptr = new type(arg1)
#define fast_new2(type,ptr,arg1,arg2) ptr = new type(arg1,arg2)
#define fast_new3(type,ptr,arg1,arg2,arg3) ptr = new type(arg1,arg2,arg3)
#define fast_new4(type,ptr,arg1,arg2,arg3,arg4) ptr = new type(arg1,arg2,arg3,arg4)
#define fast_new5(type,ptr,arg1,arg2,arg3,arg4,arg5) ptr = new type(arg1,arg2,arg3,arg4,arg5)

#define long_new(mem,type,ptr) fast_new(type,ptr)
#define long_new1(mem,type,ptr,arg1) fast_new1(type,ptr,arg1)
#define long_new2(mem,type,ptr,arg1,arg2) fast_new2(type,ptr,arg1,arg2)
#define long_new3(mem,type,ptr,arg1,arg2,arg3) fast_new3(type,ptr,arg1,arg2,arg3)
#define long_new4(mem,type,ptr,arg1,arg2,arg3,arg4) fast_new4(type,ptr,arg1,arg2,arg3,arg4)
#define long_new5(mem,type,ptr,arg1,arg2,arg3,arg4,arg5) fast_new5(type,ptr,arg1,arg2,arg3,arg4,arg5)

#define fast_delete(type,ptr) delete ptr

#else

#ifdef MEMORY_DEBUG
#define fast_alloc(type,count) (type*)_core->GetMemory()->Alloc(sizeof(type)*count,__FILE__,__FUNCTION__,__LINE__)
#define fast_free(ptr)         _core->GetMemory()->Free((void*&)ptr,__FILE__,__FUNCTION__,__LINE__)
#define long_alloc(mem,type,count) (type*)mem->Alloc(sizeof(type)*count,__FILE__,__FUNCTION__,__LINE__)
#define long_free(mem,ptr)         mem->Free((uint8_t*&)ptr,__FILE__,__FUNCTION__,__LINE__)
#else
#define fast_alloc(type,count) (type*)_core->GetMemory()->Alloc(sizeof(type)*count)
#define fast_free(ptr)         _core->GetMemory()->Free((void*&)ptr)
#define long_alloc(mem,type,count) (type*)mem->Alloc(sizeof(type)*count)
#define long_free(mem,ptr)         mem->Free((void*&)ptr)
#endif


#define fast_new(type,ptr) do{ptr = fast_alloc(type,1); new (ptr)type;}while(0)
#define fast_new1(type,ptr,arg1) 		do{ptr = fast_alloc(type,1); new (ptr)type(arg1);}while(0)
#define fast_new2(type,ptr,arg1,arg2) 		do{ptr = fast_alloc(type,1); new (ptr)type(arg1,arg2);}while(0)
#define fast_new3(type,ptr,arg1,arg2,arg3) 	do{ptr = fast_alloc(type,1); new (ptr)type(arg1,arg2,arg3);}while(0)
#define fast_new4(type,ptr,arg1,arg2,arg3,arg4)	do{ptr = fast_alloc(type,1); new (ptr)type(arg1,arg2,arg3,arg4);}while(0)
#define fast_new5(type,ptr,arg1,arg2,arg3,arg4,arg5)	do{ptr = fast_alloc(type,1); new (ptr)type(arg1,arg2,arg3,arg4,arg5);}while(0)

#define fast_delete(type,ptr)			do{ptr->~type(); fast_free(ptr);}while(0)

#define long_new (mem,type,ptr) 			do{ptr = long_alloc(mem,type,1); new (ptr)type;}while(0)
#define long_new1(mem,type,ptr,arg1) 		do{ptr = long_alloc(mem,type,1); new (ptr)type(arg1);}while(0)
#define long_new2(mem,type,ptr,arg1,arg2) 		do{ptr = long_alloc(mem,type,1); new (ptr)type(arg1,arg2);}while(0)
#define long_new3(mem,type,ptr,arg1,arg2,arg3) 	do{ptr = long_alloc(mem,type,1); new (ptr)type(arg1,arg2,arg3);}while(0)
#define long_new4(mem,type,ptr,arg1,arg2,arg3,arg4)	do{ptr = long_alloc(mem,type,1); new (ptr)type(arg1,arg2,arg3,arg4);}while(0)
#define long_new5(mem,type,ptr,arg1,arg2,arg3,arg4,arg5)	do{ptr = long_alloc(mem,type,1); new (ptr)type(arg1,arg2,arg3,arg4,arg5);}while(0)

#define long_delete(mem,type,ptr)			do{ptr->~type(); long_free(mem,ptr);}while(0)

#endif

#endif