#include "PointerInfo.h"

PointerInfo::PointerInfo(size_t size)
{
    _ptr    = malloc(size);
    _size   = size;
    _freed  = false;
}

PointerInfo::~PointerInfo()
{
    free(_ptr);    
}

#ifdef MEMORY_DEBUG
void PointerInfo::SetDebugInfo(const char *filename, const char *function, int line, const char *time)
{
    _filename.assign(filename);
    _function.assign(function);
    _time.assign(time);
    _line = line;
}
#endif

