#ifndef POINTERINFO__H
#define POINTERINFO__H

#include "../../common/config.h"
#include <string>

class PointerInfo
{
public:
    PointerInfo(size_t size);
    ~PointerInfo();
    
    inline bool     IsFreed()   {return _freed;}
    inline void*    GetPtr()    {return _ptr;}
    inline size_t   GetSize()   {return _size;}
    
    inline void     SetFreed(bool value) {_freed = value;}
    
#ifdef MEMORY_DEBUG
    void SetDebugInfo(const char *filename,const char* function, int line, const char *time);

    
    inline const char*     GetFilename() {return _filename.c_str();}
    inline const char*     GetFunction() {return _function.c_str();}
    inline int             GetLine()     {return _line;}
    inline const char*     GetTime()     {return _time.c_str();}
#endif    
private:
    
#ifdef MEMORY_DEBUG
    std::string _filename;
    std::string _function;
    int         _line;
    std::string _time;
#endif
    
    void        *_ptr;
    size_t      _size;
    bool        _freed;    
};

#endif