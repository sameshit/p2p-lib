#include "UDPSocket.h"

UDPSocket::UDPSocket(CoreObject * core)
:BaseEventHandler(core->GetLoop(),socket( PF_INET,SOCK_DGRAM,0))
{
	_core = core;
	assert(_fd >= 0);
}

UDPSocket::~UDPSocket()
{
	close(_fd);
}



