#ifndef SOLIDUDPSOCKET_H
#define SOLIDUDPSOCKET_H

#include "../../CoreObject.h"
#include "../../basefd/BaseEventHandler.h"

class UDPSocket
: public BaseEventHandler
{
public:
	UDPSocket(CoreObject *core);
	virtual ~UDPSocket();
protected:
	CoreObject *_core;
};

#endif
