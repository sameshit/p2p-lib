#ifndef TCPCONNECTIONMANAGER__H
#define TCPCONNECTIONMANAGER__H

#include "TCPSocket.h"
#include "TCPConnection.h"
#include <map>

class TCPConnectionManager 
:public TCPSocket
{
public:
    TCPConnectionManager(CoreObject *core);
    virtual ~TCPConnectionManager();
    TCPConnection* CreateConnection(int fd,uint32_t host,uint16_t port);    
    TCPConnection* FindConnection(int fd);
    void DeleteConnection(int fd);
    inline void DeleteConnection(TCPConnection *connection) {DeleteConnection(connection->_fd);}
    virtual void OnDeleteConnection(TCPConnection* connection);
protected:    
    std::map<int,TCPConnection *> _connections;    
private:    
    static inline void DisconnectionCallback(void *ptr,TCPConnection* connection,int fd) {TCPConnectionManager *manager = (TCPConnectionManager*)ptr; manager->OnDeleteConnection(connection); manager->DeleteConnection(fd);}
};

#endif