#include "TCPListener.h"

TCPListener::TCPListener(CoreObject *core,uint16_t port)
:TCPConnectionManager(core)
{    
    int flags;
    int on;
    
    _addr.sin_family        = AF_INET;
    _addr.sin_port          = htons(port);
    _addr.sin_addr.s_addr   = htonl(INADDR_ANY); 
    
    on = 1;
    assert(setsockopt( _fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on) ) == 0);
    
    flags = fcntl(_fd, F_GETFL, 0);
    assert(flags != -1);
    fcntl(_fd, F_SETFL, flags | O_NONBLOCK);
    
    assert(bind(_fd,(struct sockaddr *)&_addr,sizeof(_addr)) == 0);    
        
    assert(listen(_fd,1) == 0);
    
    LOG << "Bind called on port " << port;
}

TCPListener::~TCPListener()
{

}

void TCPListener::OnRead()
{
    struct sockaddr_in in_addr;
    socklen_t in_len = sizeof(in_addr);     
    int recvd_fd;
    TCPConnection *connection;
    
    while (1)
    {
        recvd_fd = accept(_fd, (struct sockaddr *)&in_addr, &in_len);
    
        if (recvd_fd == -1)
        {
            if (errno == EWOULDBLOCK)
            {
//                LOG("Accept would block");
                break; 
            }
            else
                assert(false);
        }
        connection = CreateConnection(recvd_fd, in_addr.sin_addr.s_addr, in_addr.sin_port);        
        connection->SetNewMessageCallback(TCPListener::StaticOnNewMessage, this);
        OnNewConnection(connection);
    }    
}

void TCPListener::OnNewConnection(TCPConnection *connection)
{
    LOG << "Someone connected" << std::endl;
}

void TCPListener::OnNewMessage(TCPConnection *connection, uint8_t *data, ssize_t size)
{
    LOG << "New message recevd " << (char *)data << std::endl;
}
