#ifndef TCPCONNECTION__H
#define TCPCONNECTION__H

#include "../../basefd/BaseEventHandler.h"
#include "../../CoreObject.h"

#define TCPCONNECTION_MAX_MSG_SIZE 65536

#define TCP_ANY_MSG_SIZE   1
#define TCP_FIXED_MSG_SIZE 2

class TCPConnection
:public BaseEventHandler
{
public:
    TCPConnection(CoreObject *core,int fd,uint32_t host,uint16_t port);
    virtual ~TCPConnection();
    virtual void    OnRead();
    void    SetDisconnectionCallback(void (*callback)(void *,TCPConnection *,int),void *data);
    void    SetNewMessageCallback(void (*callback)(void*,TCPConnection *,uint8_t *,ssize_t),void *data);
    
    inline int GetMessageSize()                         {return _message_size;}
    void SetMessageSize(ssize_t size);            
    
    inline int GetMessageMode()                         {return _message_mode;}
    inline void SetMessageMode(uint8_t message_mode)    {_message_mode = message_mode;}
private:
    CoreObject *_core;
    uint32_t _host;
    uint16_t _port;
    uint8_t  *_buffer;
    void (*_disconnect_cb)(void *,TCPConnection *,int);
    void *_disconnect_data;
    int _flags;
    ssize_t _message_size;
    ssize_t _need_size;
    
    void (*_new_message_cb)(void *,TCPConnection *,uint8_t *,ssize_t);
    void *_new_message_data;
    uint8_t _message_mode;
    uint8_t *_pbuf;
    friend class TCPConnectionManager;
};

#endif