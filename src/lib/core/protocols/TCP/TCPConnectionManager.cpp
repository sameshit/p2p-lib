#include "TCPConnectionManager.h"

TCPConnectionManager::TCPConnectionManager(CoreObject *core)
:TCPSocket(core)
{

}

TCPConnectionManager::~TCPConnectionManager()
{

}

TCPConnection* TCPConnectionManager::CreateConnection(int fd,uint32_t host, uint16_t port)
{
    TCPConnection *connection;
    
    fast_new4(TCPConnection,connection,_core, fd, host, port);
    
    _connections.insert(std::make_pair<int,TCPConnection* >(fd,connection));
    connection->SetDisconnectionCallback(TCPConnectionManager::DisconnectionCallback, this);
    
    return connection;
}

TCPConnection* TCPConnectionManager::FindConnection(int fd)
{
    std::map<int, TCPConnection*>::iterator it;
    
    it = _connections.find(fd);
    
    if (it != _connections.end())
        return (*it).second;
    else
        return NULL;
}

void TCPConnectionManager::DeleteConnection(int fd)
{
    std::map<int, TCPConnection*>::iterator it;
    
    it = _connections.find(fd);
    if (it != _connections.end())
    {
        fast_delete(TCPConnection,(*it).second);
        _connections.erase(it);
    }
    else
        LOG << "Deleting unexisting connection " << fd << std::endl;    
}

void TCPConnectionManager::OnDeleteConnection(TCPConnection *connection)
{
    LOG << "Someone disconnected (host:"<<connection->_host<< ",port:"<< connection->_port <<")"<<std::endl;
}
