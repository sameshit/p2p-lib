#include "TCPConnection.h"

TCPConnection::TCPConnection(CoreObject *core, int fd,uint32_t host,uint16_t port)
:BaseEventHandler(core->GetLoop(),fd)
{
    int fcntl_flags;    
    _core = core;
    _host = host;
    _port = port;
    
    fcntl_flags = fcntl(_fd, F_GETFL, 0);
    assert(fcntl_flags != -1);
    fcntl(_fd, F_SETFL, fcntl_flags | O_NONBLOCK);
    
    _disconnect_cb = NULL;
    _disconnect_data = NULL;
    
    _new_message_cb = NULL;
    _new_message_data = NULL;    
    
    _message_size = TCPCONNECTION_MAX_MSG_SIZE;
    _need_size    = TCPCONNECTION_MAX_MSG_SIZE;
    _buffer       = fast_alloc(uint8_t, _message_size);
    
    _message_mode = TCP_ANY_MSG_SIZE;
    _pbuf = _buffer;
}

TCPConnection::~TCPConnection()
{
    shutdown(_fd, SHUT_RDWR);
    
    fast_free(_buffer);
}

void TCPConnection::OnRead()
{
    ssize_t bytes_read;
    uint8_t *data;

    
    while (1)
    {
        bytes_read = recv(_fd,_pbuf,_need_size,MSG_DONTWAIT);
        if (bytes_read <= 0)
        {
            if (bytes_read == -1 && errno == EAGAIN)
                break;
            else
            {            
                if (_disconnect_cb != NULL)            
                    _disconnect_cb(_disconnect_data,this,_fd);
                else
                    LOG << "Disconnection callback wasn't set" << std::endl;
                break;
            }
        }
        
        if (_new_message_cb == NULL)
        {
            LOG << "New message callback wasn't set" << std::endl;
            return;
        }
        
        if (_message_mode == TCP_ANY_MSG_SIZE)
        {
            data = fast_alloc(uint8_t, bytes_read);
            memcpy (data,_buffer,bytes_read);        
            _new_message_cb(_new_message_data,this,data,bytes_read);        
            fast_free(data);        
        }
        else if (_message_mode == TCP_FIXED_MSG_SIZE)
        {
                if (bytes_read != _need_size)
                {
                    _pbuf = &_pbuf[bytes_read];
                    _need_size -= bytes_read;
                }
                else
                {
                    _need_size = _message_size;
                    _pbuf      = _buffer;
                    data = fast_alloc(uint8_t,_message_size);
                    memcpy(data,_buffer,_message_size);                    
                    _new_message_cb(_new_message_data,this,data,_message_size);
                    fast_free(data);
                }
        }
    }  
}

void TCPConnection::SetDisconnectionCallback(void (*callback)(void*,TCPConnection *,int),void *data)
{
    _disconnect_cb      = callback;
    _disconnect_data    = data;
}

void TCPConnection::SetNewMessageCallback(void (*callback)(void*,TCPConnection *,uint8_t *,ssize_t),void *data)
{
    _new_message_cb   = callback;
    _new_message_data = data;
}

void TCPConnection::SetMessageSize(ssize_t size)
{
    _message_size = size;
    _need_size = _message_size;
    fast_free(_buffer);
    _buffer = fast_alloc(uint8_t,_message_size);
    _pbuf = _buffer;
}
