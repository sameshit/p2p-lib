#include "TCPSocket.h"

TCPSocket::TCPSocket(CoreObject *core)
:BaseEventHandler(core->GetLoop(),socket(PF_INET,SOCK_STREAM,0))
{
    _core = core;
}

TCPSocket::~TCPSocket()
{
    shutdown(_fd, SHUT_RDWR);
}