#ifndef LIVETCPLISTENER__H
#define LIVETCPLISTENER__H

#include "TCPConnectionManager.h"

class TCPListener 
:public TCPConnectionManager
{
public:
    TCPListener(CoreObject *core,uint16_t port);
    virtual ~TCPListener();
    void OnRead();
    virtual void OnNewMessage(TCPConnection *connection,uint8_t* data, ssize_t size);
    virtual void OnNewConnection(TCPConnection *connection);
private:
    struct sockaddr_in _addr;
    
    static inline void StaticOnNewMessage(void *ptr,TCPConnection *connection,uint8_t *data, ssize_t size)
    {
        TCPListener *listener = (TCPListener *)ptr;
        listener->OnNewMessage(connection,data,size);
    }
};

#endif