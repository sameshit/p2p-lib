#ifndef TCPSOCKET__H
#define TCPSOCKET__H

#include "../../CoreObject.h"
#include "../../basefd/BaseEventHandler.h"

class TCPSocket 
:public BaseEventHandler
{
public:
    TCPSocket(CoreObject *core);
    virtual ~TCPSocket();        
protected:
    CoreObject *_core;
};

#endif
