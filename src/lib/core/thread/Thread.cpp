#include "Thread.h"

int success_status = 0;

Thread::Thread()
{
	int ret;

	_mutex = new Mutex();
	_exit  = false;

#if defined(OS_LINUX) || defined(OS_X)
   	ret = pthread_create(&_thread_id,NULL,Thread::OnSystemThread,(void*)this);
	assert(ret == 0);
#endif
}

Thread::~Thread()
{
	int *status;
	int ret;

	_mutex->Lock();
	_exit = true;
	_mutex->UnLock();

#if defined(OS_LINUX) || defined(OS_X)
	ret = pthread_join(_thread_id,(void**)&status);
#endif

	delete _mutex;
}

void* Thread::OnSystemThread(void *data)
{
	Thread *thread = (Thread *)data;
	bool exit;
	while(true)
	{
		thread->_mutex->Lock();
		exit = thread->_exit;
		thread->_mutex->UnLock();

		if (exit)
			pthread_exit((void*)success_status);
		else
			thread->OnThread();
	}
}
