#ifndef THREAD___H
#define THREAD___H

#include "../../common/config.h"
#include "../event/Event.h"
#include "Mutex.h"

class Thread
{
	public:
		Thread();
		~Thread();

		Event0 OnThread;
	private:
		static void* 	OnSystemThread(void *data);
		Mutex 			*_mutex;
		bool			_exit;

		#if defined(OS_LINUX) || defined(OS_X)
		pthread_t 		_thread_id;
		#endif
};

#endif
