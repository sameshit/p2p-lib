#include "Mutex.h"

Mutex::Mutex()
{
int ret;

#if defined(OS_LINUX) || defined(OS_X)
	ret = pthread_mutex_init(&mutex,NULL);
	assert(ret == 0);
#endif
}

Mutex::~Mutex()
{
int ret;

#if defined(OS_LINUX) || defined(OS_X)
	ret = pthread_mutex_destroy(&mutex);
	assert(ret == 0);
#endif
}

void Mutex::Lock()
{
int ret;

#if defined(OS_LINUX) || defined(OS_X)
	ret = pthread_mutex_lock(&mutex);
	assert(ret == 0);
#endif
}

void Mutex::UnLock()
{
int ret;

#if defined(OS_LINUX) || defined(OS_X)
	ret = pthread_mutex_unlock(&mutex);
	assert(ret == 0);
#endif
}
