#ifndef MUTEX___H
#define MUTEX___H

#include "../../common/config.h"


class Mutex
{
	public:
		Mutex();
		~Mutex();
		void Lock();
		void UnLock();
	private:
#if defined(OS_LINUX) || defined(OS_X)
		pthread_mutex_t mutex;
#endif
};


#endif
