    #include "BufferManager.h"

BufferManager::BufferManager(CoreObject *core,uint32_t max_buffer_size)
{
    _core = core;
    _max_buffer_size = max_buffer_size;
    
    _header = NULL;
    
}

BufferManager::~BufferManager()
{
    std::map<uint32_t,uint8_t *>::iterator it;
    
    for (it = _buffer.begin(); it != _buffer.end(); it ++)
        fast_free((*it).second);

    _buffer.clear();
    
    if (_header != NULL)
        fast_free(_header);
}

void BufferManager::Add(uint32_t block_id, uint8_t *data,ssize_t size)
{
    uint8_t *put_data;
        
    if (_max_buffer_size != 0)
    {
        if (_buffer.size() > _max_buffer_size)
            Delete((*_buffer.begin()).first);
    }
    
    put_data = fast_alloc(uint8_t, size);
    memcpy(put_data,data,size);
    
    assert(Exists(block_id) == false);
    _buffer.insert(std::make_pair(block_id, put_data));
}

bool BufferManager::Delete(uint32_t block_id)
{
    std::map<uint32_t,uint8_t *>::iterator it;
    
    it = _buffer.find(block_id);
    
    if (it == _buffer.end())
    {
        LOG << "Invalid block id" << std::endl;
        return false;
    }
    
    fast_free((*it).second);
    _buffer.erase(it);  
    
    return true;
}

bool BufferManager::Exists(uint32_t block_id)
{
    std::map<uint32_t,uint8_t *>::iterator it;
    
    it = _buffer.find(block_id);
    
    if (it == _buffer.end())
        return false;
    
    return true;
}

uint8_t* BufferManager::Find(uint32_t block_id)
{
    std::map<uint32_t,uint8_t *>::iterator it;
    
    it = _buffer.find(block_id);
    
    if (it == _buffer.end())
        return NULL;
    else
        return (*it).second;
}

void BufferManager::AddHeader(uint8_t *header,ssize_t size)
{
    if (_header != NULL)
        fast_free(_header);
    
    _header = fast_alloc(uint8_t, size);
    memcpy(_header,header,size);
}
