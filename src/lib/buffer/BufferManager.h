#ifndef BUFFERMANAGER__H
#define BUFFERMANAGER__H

#include <map>
#include "../core/CoreObject.h"


class BufferManager 
{
public:
    BufferManager(CoreObject *core,uint32_t max_buffer_size = 0);
    
    void Add(uint32_t block_id,uint8_t *data,ssize_t size);
    void AddHeader(uint8_t *header,ssize_t size);
    bool Delete(uint32_t block_id);
    bool Exists(uint32_t block_id);    
    uint8_t *Find(uint32_t block_id);
    virtual ~BufferManager();
private:
    CoreObject *_core;
    std::map<uint32_t,uint8_t*> _buffer;
    uint32_t _max_buffer_size;
    uint8_t *_header;
};    

#endif
