#ifndef P2PCONFIG_H
#define P2PCONFIG_H

#if defined(OS_LINUX)
#define __STDC_LIMIT_MACROS
#define __STDC_FORMAT_MACROS
#endif

#include </usr/include/ev.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <time.h>
#include <map>
#include <set>
#include <iostream>
#include <fstream>

#if defined(OS_LINUX) || defined(OS_X)
#include "unix/config.h"
#endif

#endif
