#include "SolidUDPIO.h"

SolidUDPIO::SolidUDPIO(CoreObject *core,uint16_t port)
:SolidUDPSessionManager(core)
{
    int flags;    
    
	_addr.sin_family = AF_INET;
	_addr.sin_port = htons( port );
	_addr.sin_addr.s_addr = htonl( INADDR_ANY );
    
	if(bind(_fd, (struct sockaddr *)&_addr, sizeof(_addr)) != 0)
    {
        LOG<< "Couldn't bind socket on "<<inet_ntoa(_addr.sin_addr)<<":"<<port<<std::endl;
        exit(1);
    }
    
    LOG <<"Binded socket on "<<inet_ntoa(_addr.sin_addr)<<":"<<port<<std::endl;
    
    flags = fcntl(_fd, F_GETFL, 0);
    assert(flags != -1);
    fcntl(_fd, F_SETFL, flags | O_NONBLOCK);
    
    _total_bytes_read       = 0;
    _total_bytes_write      = 0;
    _total_packets_recvd    = 0;
    _total_packets_sent     = 0;
    
    _total_bytes_read_speed     = 0;
    _total_bytes_write_speed    = 0;
    _total_packets_sent_speed   = 0;
    _total_packets_recvd_speed  = 0;
    
    _timer.data = this;
    ev_timer_init(&_timer, SolidUDPIO::CalcSpeedTimeout, 1., 1.);
    ev_timer_start(_core->GetLoop(), &_timer);
}


SolidUDPIO::~SolidUDPIO()
{
    ev_timer_stop(_core->GetLoop(), &_timer);
}

void SolidUDPIO::CalcSpeedTimeout(struct ev_loop *loop, ev_timer *w, int revents)
{
    SolidUDPIO *udp_io = (SolidUDPIO *)w->data;    
    std::map<uint64_t, SolidUDPSession* >::iterator it;
    SolidUDPSession *session;
    uint32_t connections_count = 0;

    udp_io->_total_bytes_read_speed     = 0;
    udp_io->_total_bytes_write_speed    = 0;
    udp_io->_total_packets_recvd_speed  = 0;
    udp_io->_total_packets_sent_speed   = 0;
  
    for (it = udp_io->_sessions.begin(); it != udp_io->_sessions.end(); it ++)
    {
            session = (*it).second;
            
            session->_bytes_read_speed      = session->_bytes_read;
            session->_bytes_write_speed     = session->_bytes_write;
            session->_packets_recvd_speed   = session->_packets_recvd;
            session->_packets_sent_speed    = session->_packets_sent;
            
            session->_bytes_read        = 0;
            session->_bytes_write       = 0;
            session->_packets_recvd     = 0;
            session->_packets_sent      = 0;          
            

		    udp_io->_total_bytes_read_speed     += session->_bytes_read_speed;
		    udp_io->_total_bytes_write_speed    += session->_bytes_write_speed;
		    udp_io->_total_packets_recvd_speed  += session->_packets_recvd_speed;
		    udp_io->_total_packets_sent_speed   += session->_packets_sent_speed;
    
            connections_count ++;
    }
    
    LONG_LOG(udp_io->_core->GetLogger())<<"("<<connections_count<<" connections)Out speed: "<<udp_io->_total_bytes_write_speed <<" b\\s, "<<udp_io->_total_packets_sent_speed<<" packets\\s. In speed: "<< udp_io->_total_bytes_read_speed<<" b\\s, "<< udp_io->_total_packets_recvd_speed <<" packets\\s"<<std::endl;
}

