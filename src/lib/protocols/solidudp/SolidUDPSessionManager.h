#ifndef SOLIDUDPSESSIONMANAGER__H
#define SOLIDUDPSESSIONMANAGER__H

#include "SolidUDPSession.h"
#include "../../core/protocols/UDP/UDPSocket.h"
#include <map>

class SolidUDPSessionManager
:public UDPSocket
{
public:    
    SolidUDPSessionManager(CoreObject *core);
    virtual ~SolidUDPSessionManager();
    
    SolidUDPSession* FindSession(uint32_t &ip, uint16_t &port);
    SolidUDPSession* CreateSession(uint32_t ip, uint16_t port);
    
    void DeleteSession(SolidUDPSession* );
    
    Event1<SolidUDPSession*>    OnDisconnect;
    Event1<SolidUDPSession*>    OnConnect;
    
    inline uint32_t GetConnectionsCount() {return _sessions_count;}
protected:
    std::map<uint64_t,SolidUDPSession *> _sessions;
private:

    void Connected      (SolidUDPSession *session);
    void Disconnected   (SolidUDPSession *session);
    
    uint64_t GetPid(uint32_t host, uint16_t port);
    uint32_t _sessions_count;
};

#endif
