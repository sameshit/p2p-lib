#include "SolidUDPProtocol.h"
#include "../../utils/misc/Utils.h"

SolidUDPProtocol::SolidUDPProtocol(CoreObject *core,uint16_t port)
:SolidUDPIO(core,port)
{
		_connection_timer.data = this;
	 	ev_timer_init(&_connection_timer, SolidUDPProtocol::ConnectionTimeout, 1., 1.);
    	ev_timer_start(_core->GetLoop(), &_connection_timer);
}

SolidUDPProtocol::~SolidUDPProtocol()
{
		ev_timer_stop(_core->GetLoop(),&_connection_timer);
}

void SolidUDPProtocol::OnRead()
{
	ssize_t recvd_bytes;
    struct sockaddr_in in_addr;
    socklen_t in_len = sizeof(in_addr);
    SolidUDPSession *session;

	while (true)
	{
    	recvd_bytes = recvfrom(_fd,_buffer,65536,MSG_DONTWAIT,(struct sockaddr *)&in_addr,&in_len);

	    if (recvd_bytes == -1)
    	{
        	if (errno != EAGAIN && errno != EWOULDBLOCK)
            	LOG<<"Error while recving data"<<std::endl;
	        break;
    	}
	    if (recvd_bytes < 3)	
    	{
        	LOG<<"Invalid datagram"<<std::endl;
			continue;
	    }
    
	    if (in_len == 0)
	    {
    	    LOG<<"Bad recipent, "<<recvd_bytes<<" bytes received"<< std::endl;
			continue;
		}
    
	    session = FindSession(in_addr.sin_addr.s_addr, in_addr.sin_port);    
	    if (session != NULL && session->_active == false)
    	    continue;
    
	    if (session == NULL)
	        session = CreateSession(in_addr.sin_addr.s_addr, in_addr.sin_port);

    	session->_bytes_read += recvd_bytes;
	    session->_packets_recvd ++;

		time(&session->_last_read_time);
		OnSystemMessage(session,_buffer,recvd_bytes);
	}
}
void SolidUDPProtocol::OnWrite()
{    
	assert(false);
}

void SolidUDPProtocol::CheckDelivery(SolidUDPSession *from,uint16_t &delivery_id)
{	
	SolidUDPMessage *message;
	uint32_t count = 0;
        
    if (!from->_message_window->Exists(delivery_id))
    {
        LOG << "Received delivery message for unexisting packet with id: "<<delivery_id<<". Last packet id in window is "<<from->_message_window->Front()->GetId()<<", size "<< from->_message_window->Size() << std::endl;
        return;
    }

	if (from->_message_window->Front()->GetId() == delivery_id)
		FlowControl(from->_message_window->Front());

	while (from->_message_window->Front()->GetId() != delivery_id)
	{
		count ++;
		message = from->_message_window->Front();
		from->SystemSend(message);
		from->_message_window->Pop();
	}

	message = from->_message_window->Front();
	if (count != 0)
	    LOG << "Lost "<< count <<" packets from " << from->GetRealHost()<<":"<< from->GetRealPort() <<". Resending them. Last known packet is " << message->GetId() << std::endl;
	assert(message->GetId() == delivery_id);
	from->_message_window->Pop();
	fast_delete(SolidUDPMessage,message);
}

void SolidUDPProtocol::OnSystemMessage(SolidUDPSession *from,uint8_t *data, ssize_t &size)
{
	SolidMessageType type = (SolidMessageType)data[0];
	uint16_t delivery_id;
	SolidUDPMessage *message;

	delivery_id = Utils::GetBe16(&data[1]);

	if (type != DeliveryMsg)
		from->Delivery(delivery_id);

	switch(type)
	{
		case PingMsg:
		break;
		case MessageMsg:
			NewMessageEvt(from,&data[3],size - 3);
		break;
		case DeliveryMsg:
			CheckDelivery(from,delivery_id);
		break;
		case DisconnectMsg:
			from->_active = false;
		break;
		default:
			LOG << "Unknown message type from "<<from->GetRealHost()<<":"<<from->GetRealPort()<<". Disconnecting from this session" << std::endl;            
			from->_active = false;
		break;
	}
}

void SolidUDPProtocol::ConnectionTimeout (struct ev_loop *loop, ev_timer *w, int revents)
{
	SolidUDPProtocol 									*protocol = (SolidUDPProtocol *)w->data;
	std::map<uint64_t,SolidUDPSession *>::iterator 		it;
	std::queue<SolidUDPSession *>						sessions_for_delete;
	SolidUDPSession*									session;
	time_t												now;

	time(&now);
	
	for (it = protocol->_sessions.begin(); it != protocol->_sessions.end(); it ++)
	{
		session = (*it).second;
		if (difftime(now,session->_last_read_time) > SOLID_CONNECTION_TIMEOUT)
			sessions_for_delete.push(session);
		else
			session->Ping();
	}

	while (sessions_for_delete.size() > 0)
	{
		protocol->DeleteSession(sessions_for_delete.front());
		sessions_for_delete.pop();
	}
}

void SolidUDPProtocol::FlowControl(SolidUDPMessage *message)
{
	struct timeval now;
	suseconds_t recv_rate;
	
	gettimeofday(&now,NULL);
	
	recv_rate = Utils::DiffTimeMilli(now,_last_recv_time);

	if (message->_sending_rate == 0)
		return;

	if (recv_rate < message->_sending_rate)
		LOG << "Flow congestion occured" << std::endl;

	_last_recv_time = now;
}
