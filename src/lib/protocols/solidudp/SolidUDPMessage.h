#ifndef SOLIDUDPMESSAGE___H
#define SOLIDUDPMESSAGE___H

#include "../../core/CoreObject.h"
#include "../../utils/misc/Utils.h"

enum SolidMessageType
{
	PingMsg 		= 1,
	DeliveryMsg 	= 2,
	MessageMsg		= 3,
	DisconnectMsg	= 4
};

class SolidUDPMessage 
{
public:
    SolidUDPMessage(CoreObject *core,SolidMessageType type,uint8_t *data,ssize_t size);
    virtual ~SolidUDPMessage();
        
private:
    inline void 	SetId(uint16_t id) 	{Utils::PutBe16(&_data[1],id);}
	inline uint16_t GetId() 			{return Utils::GetBe16(&_data[1]);}

    CoreObject  	*_core;
    uint8_t     	*_data;
    ssize_t     	_total_size;
	struct timeval 	_sent_time;
	suseconds_t		_sending_rate;
    
    friend class SolidUDPSession;
    friend class SolidUDPDelivery;
    friend class SolidUDPIO;
	friend class SolidUDPProtocol;
	friend class SolidUDPWindow;
};

#endif
