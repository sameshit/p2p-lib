#ifndef SOLIDUDPCONNECTION__H
#define SOLIDUDPCONNECTION__H

#include "../../core/CoreObject.h"
#include "SolidUDPMessage.h"
#include "SolidUDPWindow.h"
#include <queue>
#include <map>

#define SOLID_CONNECTION_TIMEOUT 2.

class SolidUDPSession 
{
public:
        
    bool Send(uint8_t *data, size_t size);
    
    inline const char* GetRealHost()    {return inet_ntoa(_addr.sin_addr);}   
    inline uint16_t    GetRealPort()    {return ntohs(_addr.sin_port);}
    
    inline uint32_t    GetNetworkHost() {return _addr.sin_addr.s_addr;}
    inline uint16_t    GetNetworkPort() {return _addr.sin_port;}
    
    void Disconnect();
private:
    SolidUDPSession(CoreObject *core,uint32_t host,uint16_t port,int &fd);
    virtual ~SolidUDPSession();
	
	bool SystemSend(SolidUDPMessage *message);
	void Ping();
	void Delivery(uint16_t id);

    CoreObject                  			*_core;
    struct sockaddr_in          			_addr;    
    uint16_t                    			_last_message_id;
	SolidUDPWindow							*_message_window;

	time_t									_last_read_time;
	SolidUDPMessage							*_last_sent_message;

    bool _active;
    int _fd;
    
    uint64_t _bytes_read,_bytes_write;
    uint64_t _packets_sent,_packets_recvd;
    uint64_t _bytes_read_speed,_bytes_write_speed;
    uint64_t _packets_sent_speed,_packets_recvd_speed;
    
    friend class SolidUDPProtocol;
    friend class SolidUDPIO;
    friend class SolidUDPSessionManager;
};

#endif
