#ifndef SOLIDUDPIO__H
#define SOLIDUDPIO__H

#include "../../core/protocols/UDP/UDPSocket.h"
#include "SolidUDPSessionManager.h"

class SolidUDPIO 
:public SolidUDPSessionManager
{
public:
    SolidUDPIO(CoreObject *core,uint16_t port);
	virtual ~SolidUDPIO();
    
    inline uint16_t GetRealPort()       {return ntohs(_addr.sin_port);}
    inline uint16_t GetNetworkPort()    {return _addr.sin_port;}
    
    inline void StopSpeedCalcTimer()           {ev_timer_stop(_core->GetLoop(), &_timer);}
    inline void StartSpeedCalcTimer()          {ev_timer_start(_core->GetLoop(),&_timer);}
private:
    static void CalcSpeedTimeout (struct ev_loop *loop, ev_timer *w, int revents);

protected:    
    uint64_t _total_bytes_read,_total_bytes_write;
    uint64_t _total_packets_sent,_total_packets_recvd;
    uint64_t _total_bytes_read_speed, _total_bytes_write_speed;
    uint64_t _total_packets_sent_speed,_total_packets_recvd_speed;

private:
    struct sockaddr_in _addr;
    ev_timer _timer;
    uint16_t _port;
};

#endif
