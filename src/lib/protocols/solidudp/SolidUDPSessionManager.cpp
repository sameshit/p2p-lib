#include "SolidUDPSessionManager.h"

SolidUDPSessionManager::SolidUDPSessionManager(CoreObject *core)
:UDPSocket(core)
{
    _core = core;
    
    OnConnect.   Attach<SolidUDPSessionManager >(this, &SolidUDPSessionManager::Connected);
    OnDisconnect.Attach<SolidUDPSessionManager >(this, &SolidUDPSessionManager::Disconnected);    
    
    _sessions_count = 0;
}

SolidUDPSessionManager::~SolidUDPSessionManager()
{
    std::map<uint64_t, SolidUDPSession *>::iterator it;
    
    for (it = _sessions.begin(); it != _sessions.end(); it ++)
    {
        fast_delete(SolidUDPSession,(*it).second);
    }
}

SolidUDPSession* SolidUDPSessionManager::FindSession(uint32_t &host, uint16_t &port)
{
    std::map<uint64_t,SolidUDPSession *>::iterator it;
    uint64_t pid = GetPid(host,port);
    
    it = _sessions.find(pid);
    
    if (it == _sessions.end())
        return NULL;
        
    return (*it).second;
}

SolidUDPSession* SolidUDPSessionManager::CreateSession(uint32_t host,uint16_t port)
{
    std::map<uint64_t,SolidUDPSession *>::iterator it;
    SolidUDPSession *session;
    uint64_t    pid; 
    
    session = FindSession(host,port);
    
    if (session != NULL)
    {
        LOG << "Session already exists" << std::endl;
        return session;
    }
    
    pid = GetPid(host,port);
    
    fast_new4(SolidUDPSession,session,_core,host,port,_fd);
    
    _sessions.insert(std::make_pair(pid,session));
    _sessions_count++;
        
    OnConnect(session);
    return session;
}

void SolidUDPSessionManager::DeleteSession(SolidUDPSession *session)
{
    std::map<uint64_t,SolidUDPSession *>::iterator it;
    uint64_t pid;
    
    pid = GetPid(session->GetNetworkHost(),session->GetNetworkPort());
    
    it = _sessions.find(pid);
    
    if (it == _sessions.end())
    {
        LOG << "No such session host:"<<session->GetRealHost()<<", port:"<< session->GetRealPort() << std::endl;
        return;
    }
    
    session = (*it).second;
    _sessions.erase(it);
    
    _sessions_count--;
    
    OnDisconnect(session);
    fast_delete(SolidUDPSession,session);    
}

void SolidUDPSessionManager::Connected(SolidUDPSession *session)
{
    LOG << "Connected to: "<<session->GetRealHost()<<":" <<session->GetRealPort()<< std::endl;
}

void SolidUDPSessionManager::Disconnected(SolidUDPSession *session)
{
    LOG << "Disconnected from: "<<session->GetRealHost()<<":"<<session->GetRealPort() << std::endl;
}

uint64_t SolidUDPSessionManager::GetPid(uint32_t host, uint16_t port)
{
    uint64_t            pid = 0;    
    uint8_t             *ppid =(uint8_t *)&pid;    
    
    memcpy(&ppid[0],&host,4);
    memcpy(&ppid[4],&port,2);
    
    return pid;
}
