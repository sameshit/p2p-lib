#ifndef SOLIDUDPWINDOW__H
#define SOLIDUDPWINDOW__H

#include "SolidUDPMessage.h"
#include <set>
#include <queue>


class SolidUDPWindow
{
public:
	SolidUDPWindow(CoreObject *core);
	~SolidUDPWindow();

	void Push(SolidUDPMessage *message);
	void Pop();
	inline SolidUDPMessage* Front() {return _message_queue.front();}
	bool Exists(uint16_t &id);
	inline size_t Size() {return _message_queue.size();}
private:
	std::set<uint16_t> 				_id_set;
	std::queue<SolidUDPMessage *>	_message_queue;
	CoreObject 						*_core;
};

#endif
