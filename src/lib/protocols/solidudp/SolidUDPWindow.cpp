#include "SolidUDPWindow.h"

SolidUDPWindow::SolidUDPWindow(CoreObject *core)
{
	_core = core;
}

SolidUDPWindow::~SolidUDPWindow()
{
	while(_message_queue.size() > 0)
	{
		fast_delete(SolidUDPMessage,_message_queue.front());
		_message_queue.pop();
	}
}

void SolidUDPWindow::Push(SolidUDPMessage *message)
{
	_message_queue.push(message);
	_id_set.insert(message->GetId());
        assert(_id_set.size() == _message_queue.size());
}

void SolidUDPWindow::Pop()
{

	assert(_id_set.end() != _id_set.find(_message_queue.front()->GetId()) );
	_id_set.erase(_message_queue.front()->GetId());
	_message_queue.pop();
        assert(_id_set.size() == _message_queue.size());
}

bool SolidUDPWindow::Exists(uint16_t &id)
{
	std::set<uint16_t>::iterator it;
        
        assert(_id_set.size() == _message_queue.size());

	it = _id_set.find(id);

	return (it != _id_set.end());
}
