#include "SolidUDPMessage.h"

SolidUDPMessage::SolidUDPMessage(CoreObject *core,SolidMessageType type,uint8_t *data,ssize_t size)
{
    _core = core;
    
    _total_size = size + 3;
    _data = fast_alloc(uint8_t, _total_size);
   
    _data[0] = type;
  	if (size > 0)
       	memcpy(&_data[3], data, size);
}

SolidUDPMessage::~SolidUDPMessage()
{
    fast_free(_data);    
}

