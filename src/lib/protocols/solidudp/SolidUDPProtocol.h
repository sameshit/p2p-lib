#ifndef SolidUDPProtocol__H
#define SolidUDPProtocol__H
#include "SolidUDPIO.h"
#include "SolidUDPSession.h"



class SolidUDPProtocol
: public SolidUDPIO
{
public:
	SolidUDPProtocol(CoreObject *core,uint16_t port);
	virtual ~SolidUDPProtocol();

	void OnRead();
	void OnWrite();

	Event3<SolidUDPSession *,uint8_t *,ssize_t> NewMessageEvt;
private:	
    void CheckDelivery(SolidUDPSession *from,uint16_t &delivery_id);

	void OnSystemMessage(SolidUDPSession *,uint8_t *data, ssize_t &size);

	ev_timer	_connection_timer;
	uint8_t		_buffer[65536];

    static void ConnectionTimeout (struct ev_loop *loop, ev_timer *w, int revents);
	void FlowControl(SolidUDPMessage *message);

	struct timeval _last_recv_time;
};

#endif
