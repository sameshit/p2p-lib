#include "SolidUDPSession.h"

SolidUDPSession::SolidUDPSession(CoreObject *core,uint32_t ip,uint16_t port, int &fd)
{
    _core = core;
    
    _addr.sin_port        = port;
    _addr.sin_addr.s_addr = ip;
    _addr.sin_family      = AF_INET;
     
    _last_message_id = 0;
    
    _bytes_read    = 0;
    _bytes_write   = 0;
    _packets_recvd = 0;
    _packets_sent  = 0;
    
    _bytes_read_speed       = 0;
    _bytes_write_speed      = 0;
    _packets_sent_speed     = 0;
    _packets_recvd_speed    = 0;
    
    _active = true;
    _fd = fd;

	time(&_last_read_time);

	_last_sent_message = NULL;

	fast_new1(SolidUDPWindow,_message_window,_core);
}

SolidUDPSession::~SolidUDPSession()
{
    SolidUDPMessage *message;
	std::map<uint16_t,SolidUDPMessage *>::iterator it;

	fast_delete(SolidUDPWindow,_message_window);
}

bool SolidUDPSession::Send(uint8_t *data,size_t size)
{
	SolidUDPMessage *message;

	fast_new4(SolidUDPMessage,message,_core,MessageMsg,data,size);
	
	return SystemSend(message);
}

bool SolidUDPSession::SystemSend(SolidUDPMessage *message)
{
    ssize_t sent_bytes;
	SolidUDPMessage *previous_message;
	std::map<uint16_t,SolidUDPMessage *>::iterator it;
	
	if (!_active)
	{
		LOG << "WARN: Trying to send message to inactive session" << std::endl;
		return false;
	}

	_last_message_id ++;
	message->SetId(_last_message_id);

    sent_bytes = sendto(_fd,message->_data,message->_total_size,MSG_DONTWAIT,(struct sockaddr*)&_addr,sizeof(_addr));
        
    if (sent_bytes != message->_total_size)
    {
        if (errno != EAGAIN || errno != EWOULDBLOCK)
            LOG << "sendto error " << errno << std::endl;
		return false;
    }
    else
    {
        _bytes_write  += sent_bytes;
        _packets_sent ++;
		gettimeofday(&message->_sent_time,NULL);

		if (_message_window->Size() != 0 && _last_sent_message != NULL)
			message->_sending_rate = Utils::DiffTimeMilli(message->_sent_time,_last_sent_message->_sent_time);
		else
			message->_sending_rate = 0;

		_message_window->Push(message);
		return true;
    }        
}

void SolidUDPSession::Disconnect()
{
    SolidUDPMessage *message;
        
    fast_new4(SolidUDPMessage,message,_core,DisconnectMsg,NULL,0);
    SystemSend(message);  
    
    _active = false;   
}

void SolidUDPSession::Ping()
{
	SolidUDPMessage *message;
	
	fast_new4(SolidUDPMessage,message,_core,PingMsg,NULL,0);
	SystemSend(message);
}

void SolidUDPSession::Delivery(uint16_t id)
{
	uint8_t data[3];

	data[0] = DeliveryMsg;
	Utils::PutBe16(&data[1],id);

	if (sendto(_fd,data,3,MSG_DONTWAIT,(struct sockaddr*)&_addr,sizeof(_addr)) != 3)
		LOG << "Delivery failed" << std::endl;
	else
	{
		_bytes_write +=3;
		_packets_sent ++;
	}
}
