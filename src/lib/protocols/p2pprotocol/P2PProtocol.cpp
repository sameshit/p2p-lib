#include "P2PProtocol.h"

P2PProtocol::P2PProtocol(CoreObject *core)
:SolidUDPProtocol(core,rand() % UINT16_MAX)
{

}

P2PProtocol::~P2PProtocol()
{


}

void P2PProtocol::SetNewNeighbor(uint32_t ip, uint16_t port)
{
    if (GetConnectionsCount() < 10)
    {
        LOG << "Connection count "<<GetConnectionsCount() << std::endl;
        CreateSession(ip, port);
    }
}