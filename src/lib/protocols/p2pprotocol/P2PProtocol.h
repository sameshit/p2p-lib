#ifndef P2PPROTOCOL__H
#define P2PPROTOCOL__H

#include "../solidudp/SolidUDPProtocol.h"

class P2PProtocol 
:public SolidUDPProtocol
{
public:
    P2PProtocol(CoreObject *core);
    virtual ~P2PProtocol(); 
        
    void SetNewNeighbor(uint32_t ip, uint16_t port);

};

#endif