#ifndef RMPPING__H
#define RMPPING__H

#include "RMPDelivery.h"

namespace RMP
{

class RMPPing
:public RMPDelivery
{
public:
	RMPPing(CoreObject *core);
	virtual ~RMPPing();
private:
	ev_timer _ping_timer;
	static void PingTimeout (struct ev_loop *loop, ev_timer *w, int revents);
};

}

#endif