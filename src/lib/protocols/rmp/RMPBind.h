#ifndef PMPBIND__H
#define PMPBIND__H

#include "../../core/protocols/UDP/UDPSocket.h"

namespace RMP
{

class RMPBind
:public UDPSocket
{
public:
	RMPBind(CoreObject *core);
	virtual ~RMPBind();
	
	bool Bind(uint16_t port);
private:
	struct sockaddr_in _addr;
};


}

#endif