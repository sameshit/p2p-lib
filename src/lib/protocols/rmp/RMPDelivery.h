#ifndef RMPDELIVERY__H
#define RMPDELIVERY__H

#include "RMPMessaging.h"

namespace RMP
{
class RMPDelivery
:public RMPMessaging
{
public:
	RMPDelivery(CoreObject *core);
	virtual ~RMPDelivery();
private:
	ev_timer _delivery_timer;
	static void DeliveryTimeout (struct ev_loop *loop, ev_timer *w, int revents);

	void DeliveryPush(RMPSession* to,uint32_t &id,RMPMessageType &type,uint8_t *data,ssize_t &size);
	void DeliveryPop(RMPSession *from,uint8_t *data,ssize_t &size);
	
	void DeliverySend(RMPSession *to,uint32_t &id,RMPMessageType &type,uint8_t *data,ssize_t &size);
};

}

#endif