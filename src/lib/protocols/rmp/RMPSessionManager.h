#ifndef RMPSessionManager__H
#define RMPSessionManager__H

#include "RMPSession.h"

namespace RMP
{
    struct sockaddr_in_less : public std::binary_function<struct sockaddr_in,struct sockaddr_in, bool>
    {
	    bool operator()(const struct sockaddr_in& addr1, const struct sockaddr_in& addr2) const
		{
		if (addr1.sin_addr.s_addr == addr2.sin_addr.s_addr)
			return addr1.sin_port < addr2.sin_port;
		else
			return addr1.sin_addr.s_addr < addr2.sin_addr.s_addr;
		}
    };
    
    struct sockaddr_in6_less : public std::binary_function<struct sockaddr_in6,struct sockaddr_in6, bool>
	{	
	    bool operator()(const struct sockaddr_in6& addr1, const struct sockaddr_in6& addr2) const
		{
		int ret = memcmp(addr1.sin6_addr.s6_addr,addr2.sin6_addr.s6_addr,16);
		
		if (ret == 0)
			return addr1.sin6_port < addr2.sin6_port;
		else
			return ret < 0;
		}
	};
	
	class RMPSessionManager
	{
		public:
			RMPSessionManager(CoreObject *core);
			virtual ~RMPSessionManager();
		
			RMPSession* Insert(struct sockaddr_in &addr);
			RMPSession* Insert(struct sockaddr_in6 &addr);
		
			void Delete(RMPSession *session);
		
			RMPSession* Find(struct sockaddr_in &addr);
			RMPSession* Find(struct sockaddr_in6 &addr);
			
			void GetAllSessions(std::set<RMPSession *> &session_set);
		private:
			CoreObject *_core;
			
			std::map<sockaddr_in,RMPSession *,sockaddr_in_less>  _ipv4_sessions;
			std::map<sockaddr_in6,RMPSession *,sockaddr_in6_less> _ipv6_sessions;
	};
}

#endif