#ifndef RMPNODE__H
#define RMPNODE__H

#include "RMPPing.h"

namespace RMP
{

class RMPNode
:public RMPPing
{
public:
	RMPNode(CoreObject *core);
	virtual ~RMPNode();
	
	RMPSession* Connect(struct sockaddr_in &addr);
	RMPSession* Connect(struct sockaddr_in6 &addr);
	RMPSession* Connect(const char *ip,uint16_t port,bool ipv4 = true);
};

}

#endif