#include "RMPConnection.h"

using namespace RMP;
using namespace std;

RMPConnection::RMPConnection(CoreObject *core)
:RMPIO(core)
{
	_conn_timer.data = this;
	ev_timer_init(&_conn_timer, RMPConnection::ConnectionTimeout, RMP_CONNECTION_TIMEOUT, RMP_CONNECTION_TIMEOUT);
    ev_timer_start(_core->GetLoop(), &_conn_timer);
    
    OnSystemRecv.Attach<RMPConnection>(this,&RMPConnection::UpdateTimer);
}

RMPConnection::~RMPConnection()
{
	ev_timer_stop(_core->GetLoop(),&_conn_timer);
}

inline void RMPConnection::UpdateTimer(RMPSession *session,uint32_t &id,RMPMessageType &type,uint8_t *data, ssize_t &size)
{
	gettimeofday(&session->_last_read_time,NULL);
}

void RMPConnection::ConnectionTimeout (struct ev_loop *loop, ev_timer *w, int revents)
{
	RMPConnection *rmp_conn = (RMPConnection*)w->data;
	set<RMPSession*> sessions;
	set<RMPSession*>::iterator it;
	RMPSession *session;
	struct timeval now;
	
	rmp_conn->_manager->GetAllSessions(sessions);	
	gettimeofday(&now,NULL);
	
	for (it = sessions.begin(); it != sessions.end(); it ++)
	{
		session = (*it);
		if (Utils::DiffTimeMilli(now,session->_last_read_time)	 > (suseconds_t)(RMP_CONNECTION_TIMEOUT*1000))
		{
			rmp_conn->OnDisconnect(session);
			rmp_conn->_manager->Delete(session);
		}
	}
}