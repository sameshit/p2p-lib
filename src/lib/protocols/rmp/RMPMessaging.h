#ifndef RMPMESSAGING__H
#define RMPMESSAGING__H

#include "RMPConnection.h"

namespace RMP
{
class RMPMessaging
:public RMPConnection
{
public:
	RMPMessaging(CoreObject *core);
	virtual ~RMPMessaging();
	
	bool Send(RMPSession *to,uint8_t *data, ssize_t size);
	
	Event3<RMPSession *,uint8_t *,ssize_t > OnMessage;
protected:
	bool SystemSend(RMPSession *to,RMPMessageType &type,uint8_t *data, ssize_t &size);
	void ParseMessage(RMPSession *from,uint32_t &id,RMPMessageType &type,uint8_t *data,ssize_t &size);
	
	// (session,message id,type,data,size)
	Event5<RMPSession*,uint32_t &,RMPMessageType &,uint8_t *,ssize_t &> OnSystemSend; 
	Event3<RMPSession*,uint8_t *,ssize_t &> OnDeliveryMsg;
	Event1<RMPSession*> OnDisconnectReq;
};

}

#endif