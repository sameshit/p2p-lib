#include "RMPSessionManager.h"

using namespace RMP;
using namespace std;

RMPSessionManager::RMPSessionManager(CoreObject *core)
:_core(core)
{

}

RMPSessionManager::~RMPSessionManager()
{
	map<sockaddr_in,RMPSession *>::iterator it_v4;
	map<sockaddr_in6,RMPSession *>::iterator it_v6;
	
	for (it_v4 = _ipv4_sessions.begin(); it_v4 != _ipv4_sessions.end(); it_v4 ++)
		fast_delete(RMPSession,(*it_v4).second);
	
	for (it_v6 = _ipv6_sessions.begin(); it_v6 != _ipv6_sessions.end(); it_v6 ++)
		fast_delete(RMPSession,(*it_v6).second);	
}

RMPSession* RMPSessionManager::Insert(struct sockaddr_in &addr)
{
	RMPSession *session;
	
	fast_new2(RMPSession,session,_core,addr);
	
	_ipv4_sessions.insert(make_pair(addr,session));
	
	return session;
}

RMPSession* RMPSessionManager::Insert(struct sockaddr_in6 &addr)
{
	RMPSession *session;
	
	fast_new2(RMPSession,session,_core,addr);
	
	_ipv6_sessions.insert(make_pair(addr,session));
	
	return session;
}

void RMPSessionManager::Delete(RMPSession *session)
{
	map<sockaddr_in,RMPSession *>::iterator it_v4;
	map<sockaddr_in6,RMPSession *>::iterator it_v6;
	
	if (session->IsIpv4())
	{
		it_v4 = _ipv4_sessions.find(session->_ipv4_sock_addr);
		assert(it_v4 != _ipv4_sessions.end());
		_ipv4_sessions.erase(it_v4);
		fast_delete(RMPSession,session);		
	}	
	else
	{
		it_v6 = _ipv6_sessions.find(session->_ipv6_sock_addr);
		assert(it_v6 != _ipv6_sessions.end());
		_ipv6_sessions.erase(it_v6);
		fast_delete(RMPSession,session);				
	}
}

RMPSession* RMPSessionManager::Find(struct sockaddr_in &addr)
{
	map<sockaddr_in,RMPSession *>::iterator it_v4;
	
	it_v4 = _ipv4_sessions.find(addr);
	if (it_v4 == _ipv4_sessions.end())
		return NULL;
	else
		return (*it_v4).second;
}

RMPSession* RMPSessionManager::Find(struct sockaddr_in6 &addr)
{
	map<sockaddr_in6,RMPSession *>::iterator it_v6;
	
	it_v6 = _ipv6_sessions.find(addr);
	if (it_v6 == _ipv6_sessions.end())
		return NULL;
	else
		return (*it_v6).second;
}

void RMPSessionManager::GetAllSessions(set<RMPSession *> &session_set)
{
	map<sockaddr_in,RMPSession *>::iterator it_v4;
	map<sockaddr_in6,RMPSession *>::iterator it_v6;

	for (it_v4 = _ipv4_sessions.begin(); it_v4 != _ipv4_sessions.end(); it_v4 ++)
		session_set.insert((*it_v4).second);

	for (it_v6 = _ipv6_sessions.begin(); it_v6 != _ipv6_sessions.end(); it_v6 ++)
		session_set.insert((*it_v6).second);
}