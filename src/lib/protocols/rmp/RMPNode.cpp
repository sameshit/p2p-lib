#include "RMPNode.h"

using namespace RMP;

RMPNode::RMPNode(CoreObject *core)
:RMPPing(core)
{

}

RMPNode::~RMPNode()
{

}

inline RMPSession* RMPNode::Connect(struct sockaddr_in &addr)
{
	return _manager->Insert(addr);
}

inline RMPSession* RMPNode::Connect(struct sockaddr_in6 &addr)
{
	return _manager->Insert(addr);
}

RMPSession* RMPNode::Connect(const char *ip,uint16_t port,bool isipv4)
{
	struct sockaddr_in 	addr_v4;
	struct sockaddr_in6 addr_v6;
	struct in_addr in_addr_v4;
	struct in6_addr in_addr_v6;
	
	if (isipv4)
	{		
		if (inet_pton(AF_INET,ip,&in_addr_v4) < 0)
			return NULL;
		addr_v4.sin_family = AF_INET;
		addr_v4.sin_port = htons( port );
		addr_v4.sin_addr = in_addr_v4;
		
		return _manager->Insert(addr_v4);
	}
	else
	{
		if (inet_pton(AF_INET6,ip,&in_addr_v6) < 0)
			return NULL;
		addr_v6.sin6_family = AF_INET6;
		addr_v6.sin6_port = htons(port);
		addr_v6.sin6_addr = in_addr_v6;
		
		return _manager->Insert(addr_v6);
	}
}