#include "RMPPing.h"

using namespace RMP;
using namespace std;

RMPPing::RMPPing(CoreObject *core)
:RMPDelivery(core)
{
		_ping_timer.data = this;
	 	ev_timer_init(&_ping_timer, RMPPing::PingTimeout, RMP_CONNECTION_TIMEOUT/2, RMP_CONNECTION_TIMEOUT/2);
    	ev_timer_start(_core->GetLoop(), &_ping_timer);	
}

RMPPing::~RMPPing()
{

}

void RMPPing::PingTimeout (struct ev_loop *loop, ev_timer *w, int revents)
{
	RMPPing *rmp_ping = (RMPPing *)w->data;
	set<RMPSession *> sessions;
	set<RMPSession *>::iterator it;
	RMPSession *session;
	RMPMessageType ping = Ping;
    ssize_t size = 0;
    CoreObject *_core = rmp_ping->_core;
	
	rmp_ping->_manager->GetAllSessions(sessions);
	
	for (it = sessions.begin(); it != sessions.end() ; it ++)
	{
		session = (*it);
		
		if (!rmp_ping->SystemSend(session,ping,NULL,size))
			LOG << "Couldn't send ping message to "<<session->GetIp()<<":"<<session->GetPort()<<std::endl;
	}
}