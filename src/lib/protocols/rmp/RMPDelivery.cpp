#include "RMPDelivery.h"

using namespace RMP;
using namespace std;

RMPDelivery::RMPDelivery(CoreObject *core)
:RMPMessaging(core)
{
	OnSystemSend. Attach<RMPDelivery>(this,&RMPDelivery::DeliveryPush);
	OnDeliveryMsg.Attach<RMPDelivery>(this,&RMPDelivery::DeliveryPop);
	OnSystemRecv. Attach<RMPDelivery>(this,&RMPDelivery::DeliverySend);

	_delivery_timer.data = this;
	ev_timer_init(&_delivery_timer, RMPDelivery::DeliveryTimeout, RMP_CONNECTION_TIMEOUT/2, RMP_CONNECTION_TIMEOUT/2);
    ev_timer_start(_core->GetLoop(), &_delivery_timer);
}

RMPDelivery::~RMPDelivery()
{
	ev_timer_stop(_core->GetLoop(),&_delivery_timer);
}

void RMPDelivery::DeliveryPush(RMPSession* to,uint32_t &id,RMPMessageType &type,uint8_t *data,ssize_t &size)
{
	RMPMessage message;
	
	if (type==Delivery) // skip all delivery packets
		return;
	
	message.type = type;
	message.data = fast_alloc(uint8_t,size);
	memcpy(message.data,data,size);
	message.size = size;
	gettimeofday(&message.send_time,NULL);
	
	to->_delivery_messages.insert(make_pair(id,message));
}

void RMPDelivery::DeliveryPop(RMPSession *from,uint8_t *data,ssize_t &size)
{
	map<uint32_t,RMPMessage>::iterator it;
	uint8_t *this_data;
	uint32_t id;
	
	if (size != 4)
	{
		LOG << "Invalid delivery message size("<<size<<") from "<<from->GetIp().c_str()<<":"<<from->GetPort() <<". Skipping message."<< std::endl;
		return;
	}
	
	id = Utils::GetBe32(data);
	
	it = from->_delivery_messages.find(id);
	
	if (it == from->_delivery_messages.end())
	{
		LOG << "Unknown delivery message id("<<id<< ") from "<< from->GetIp() <<":"<< from->GetPort() << std::endl;
		return;
	}
	
	this_data = (*it).second.data;
	fast_free(this_data);
	
	from->_delivery_messages.erase(it);
}

void RMPDelivery::DeliveryTimeout (struct ev_loop *loop, ev_timer *w, int revents)
{
	RMPDelivery *rmp_del = (RMPDelivery*)w->data;
	set<RMPSession*> sessions;
	set<RMPSession*>::iterator it;
	RMPSession *session;
	map<uint32_t,RMPMessage>::iterator msg_it;
	struct timeval now;
	RMPMessage msg;
	set<uint32_t> delete_ids;
	set<uint32_t>::iterator del_it;
    CoreObject *_core = rmp_del->_core;
	
	gettimeofday(&now,NULL);
	
	rmp_del->_manager->GetAllSessions(sessions);
	
	for (it = sessions.begin(); it != sessions.end(); it ++)
	{
		session = (*it);
		delete_ids.clear();
		for (msg_it = session->_delivery_messages.begin(); msg_it != session->_delivery_messages.end(); msg_it ++)
		{
			msg = (*msg_it).second;
			if(Utils::DiffTimeMilli(now,msg.send_time) > RMP_CONNECTION_TIMEOUT/2)
			{
				LOG << "Lost message with id="<< (*msg_it).first <<", type="<< msg.type <<" from "<< session->GetIp() <<":"<<session->GetPort()<<". Resending it." << std::endl;
				
				rmp_del->SystemSend(session,msg.type,msg.data,msg.size);
				
				delete_ids.insert((*msg_it).first);
				
				fast_free(msg.data);
			}
		}
		
		for (del_it = delete_ids.begin(); del_it != delete_ids.end(); del_it++)
			session->_delivery_messages.erase( (*del_it) );
	}
}

void RMPDelivery::DeliverySend(RMPSession *to,uint32_t &id,RMPMessageType &type,uint8_t *data,ssize_t &size)
{
	RMPMessageType out_type;
	uint8_t out_data[4];
	ssize_t out_size = 4;

	if (type != Delivery)
	{
		out_type = Delivery;
		Utils::PutBe32(out_data,id);
		SystemSend(to,out_type,out_data,out_size);
	}
}