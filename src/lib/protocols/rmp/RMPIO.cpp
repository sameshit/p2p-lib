#include "RMPIO.h"

using namespace RMP;
using namespace std;

RMPIO::RMPIO(CoreObject *core)
:RMPBind(core)
{
	fast_new1(RMPSessionManager,_manager,_core);
}

RMPIO::~RMPIO()
{
	fast_delete(RMPSessionManager,_manager);
}


void RMPIO::OnRead()
{
	ssize_t recvd_bytes;
    struct sockaddr_in in_addr;
    socklen_t in_len = sizeof(in_addr);
    RMPSession *session;
	RMPMessageType type;
	uint32_t id;
	ssize_t total_size;
	
	while (true)
	{
    	recvd_bytes = recvfrom(_fd,_buffer,MAX_RMP_MESSAGE,MSG_DONTWAIT,(struct sockaddr *)&in_addr,&in_len);

	    if (recvd_bytes == -1)
    	{
        	if (errno != EAGAIN && errno != EWOULDBLOCK)
            	LOG << "Error while recving data" << std::endl;
	        break;
    	}
    
	    if (in_len == 0)
	    {
    	    LOG << "Bad recipent, "<< recvd_bytes <<" bytes received" << std::endl;
			continue;
		}
		
		if (recvd_bytes < 5)
		{
			LOG << "Invalid message size "<< recvd_bytes <<". Skipping this message." << std::endl;
			continue;
		}
    
	    session = _manager->Find(in_addr);    
    
	    if (session == NULL)
	    {	     
	     	session = _manager->Insert(in_addr);
	     	OnConnect(session);
	    }
		
		id 		= Utils::GetBe32(_buffer);
		type 	= (RMPMessageType)_buffer[4]; 
		
		total_size = recvd_bytes - 5;
		OnSystemRecv(session,id,type,&_buffer[5],total_size);
	}
}

void RMPIO::OnWrite()
{
	LOG << "Illegal usage of RMPIO." << std::endl;
	assert(false);
}