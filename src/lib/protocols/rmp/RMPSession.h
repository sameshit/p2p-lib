#ifndef RMPSESSION__H
#define RMPSESSION__H

#include "RMPMessage.h"

namespace RMP
{
class RMPSession
{
	public:
		RMPSession(CoreObject *core,sockaddr_in  &addr); // ipv4 version
		RMPSession(CoreObject *core,sockaddr_in6 &addr); // ipv6 version
		virtual ~RMPSession();
		
		inline bool IsIpv4() {return _isipv4;}
		
        std::string		 GetIp();
		uint16_t	 	 GetPort();
	private:
		bool 		_isipv4;
		CoreObject 	*_core;		
		
	    struct sockaddr_in  _ipv4_sock_addr;    				
	    struct sockaddr_in6 _ipv6_sock_addr;
	    
	    struct timeval _last_read_time;
	    
	    uint32_t _last_message_id;
	    
	    std::map<uint32_t,RMPMessage> _delivery_messages;
	    
	    friend class RMPSessionManager;
	    friend class RMPConnection;
	    friend class RMPDelivery;
	    friend class RMPMessaging;
};

}
#endif