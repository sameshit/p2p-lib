#ifndef RMPIO__H
#define RMPIO__H

#include "RMPBind.h"
#include "RMPSessionManager.h"

#define MAX_RMP_MESSAGE 65536

namespace RMP
{

class RMPIO
:public RMPBind
{
public:
	RMPIO(CoreObject *core);
	virtual ~RMPIO();
		
	void OnRead();
	void OnWrite();
	
	Event1<RMPSession *> OnConnect;
private:
	uint8_t 				_buffer[MAX_RMP_MESSAGE];
protected:
	Event5<RMPSession *,uint32_t &,RMPMessageType &,uint8_t* ,ssize_t &> OnSystemRecv;
	RMPSessionManager 		*_manager;
};

}

#endif