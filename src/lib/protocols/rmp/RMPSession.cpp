#include "RMPSession.h"

using namespace RMP;
using namespace std;


RMPSession::RMPSession(CoreObject *core, struct sockaddr_in &ipv4_sock_addr)
:_isipv4(true),_core(core),_ipv4_sock_addr(ipv4_sock_addr),_last_message_id(0)
{	
	gettimeofday(&_last_read_time,NULL);
}

RMPSession::RMPSession(CoreObject *core, struct sockaddr_in6 &ipv6_sock_addr)
:_isipv4(false),_core(core),_ipv6_sock_addr(ipv6_sock_addr),_last_message_id(0)
{
	gettimeofday(&_last_read_time,NULL);
}

RMPSession::~RMPSession()
{
	map<uint32_t,RMPMessage>::iterator it;
	uint8_t *data;
	
	for (it = _delivery_messages.begin(); it != _delivery_messages.end(); it ++)
	{
		data = (*it).second.data;
		fast_free(data);
	}
}

string RMPSession::GetIp()
{
	char ip_str[INET6_ADDRSTRLEN];
    string ip;
	
	if (_isipv4)
		inet_ntop(AF_INET, &_ipv4_sock_addr.sin_addr,ip_str,INET_ADDRSTRLEN);
	else 
		inet_ntop(AF_INET6,&_ipv6_sock_addr.sin6_addr,ip_str,INET6_ADDRSTRLEN);
		
    ip.assign(ip_str);
	return ip;
}

uint16_t RMPSession::GetPort()
{
	if (_isipv4)
		return ntohs(_ipv4_sock_addr.sin_port);
	else
		return ntohs(_ipv6_sock_addr.sin6_port);
}