#include "RMPBind.h"

using namespace RMP;

RMPBind::RMPBind(CoreObject *core)
:UDPSocket(core)
{

}

RMPBind::~RMPBind()
{

}

bool RMPBind::Bind(uint16_t port)
{
	_addr.sin_family = AF_INET;
	_addr.sin_port = htons( port );
	_addr.sin_addr.s_addr = htonl( INADDR_ANY );
	
	if(bind(_fd, (struct sockaddr *)&_addr, sizeof(_addr)) != 0)
        return false;

	return true;
}