#include "RMPMessaging.h"

using namespace RMP;

RMPMessaging::RMPMessaging(CoreObject *core)
:RMPConnection(core)
{
	OnSystemRecv.Attach<RMPMessaging>(this,&RMPMessaging::ParseMessage);
}

RMPMessaging::~RMPMessaging()
{

}

bool RMPMessaging::SystemSend(RMPSession *to, RMPMessageType &type, uint8_t *data, ssize_t &size)
{
	ssize_t sent_bytes,total_size = size +5;
	struct msghdr msg_hdr;
	struct iovec msg_iov[3];
	uint8_t id[4];
		
	to->_last_message_id++;

	memset(&msg_hdr,0,sizeof(msghdr));
	
	if (to->IsIpv4())
	{
		msg_hdr.msg_name 	= &to->_ipv4_sock_addr;
		msg_hdr.msg_namelen = sizeof(sockaddr_in); 
	}
	else
	{
		msg_hdr.msg_name	= &to->_ipv6_sock_addr;
		msg_hdr.msg_namelen = sizeof(sockaddr_in6);
	}
	
	Utils::PutBe32(id,to->_last_message_id);
	
	msg_iov[0].iov_base = id;
	msg_iov[0].iov_len  = 4;
	msg_iov[1].iov_base = &type;
	msg_iov[1].iov_len	= 1;
	
	if (data != NULL)
	{
		msg_iov[2].iov_base = data;
		msg_iov[2].iov_len  = size;
		msg_hdr.msg_iovlen = 3;
	}
	else
		msg_hdr.msg_iovlen = 2;
	
	msg_hdr.msg_iov = msg_iov;
	
	sent_bytes = sendmsg(_fd,(&msg_hdr),MSG_DONTWAIT);
		
	if (sent_bytes == total_size)
	{
		OnSystemSend(to,to->_last_message_id,type,data,size);
		return true;
	}
	else
		return false;
}

inline bool RMPMessaging::Send(RMPSession *to,uint8_t *data,ssize_t size)
{
	RMPMessageType type = Message;
	
	return SystemSend(to,type,data,size);
}

void RMPMessaging::ParseMessage(RMPSession *from,uint32_t &id,RMPMessageType &type,uint8_t *data,ssize_t &size)
{	
	LOG <<"Received message with id "<<id<<" and type "<< type <<std::endl;
	uint32_t delivery_id;
			
	switch(type)
	{
		case Ping:
		break;
		case Message:
			OnMessage(from,data,size);
		break;
		case Delivery:
			OnDeliveryMsg(from,data,size);
		break;
		case Disconnect:
			OnDisconnectReq(from);
		break;
		default:
			LOG << "Invalid message type("<< type <<") from "<< from->GetIp() <<":"<<from->GetPort()<<std::endl;
		break;
	}
}