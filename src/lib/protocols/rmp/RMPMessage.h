#ifndef RMPMESSAGE__H
#define RMPMESSAGE__H

#include "../../core/CoreObject.h"
#include "../../utils/misc/Utils.h"

namespace RMP
{

enum RMPMessageType
{
	Ping = 1,
	Delivery = 2,
	Message = 3,
	Disconnect = 4,
};

typedef struct RMPMessage
{
	RMPMessageType type;
	uint8_t *data;
	ssize_t size;
	struct timeval send_time;
}RMPMessage;

}

#endif