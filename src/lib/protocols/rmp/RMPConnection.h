#ifndef RMPCONNECTION__H
#define RMPCONNECTION__H

#include "RMPIO.h"

#define RMP_CONNECTION_TIMEOUT 2.

namespace RMP
{

class RMPConnection
:public RMPIO
{
public:
	RMPConnection(CoreObject *core);
	virtual ~RMPConnection();
	
	Event1<RMPSession *> OnDisconnect;
private:
	void 			UpdateTimer(RMPSession *session,uint32_t &id,RMPMessageType &type,uint8_t *data, ssize_t &size);
	ev_timer 		_conn_timer;
	static void 	ConnectionTimeout (struct ev_loop *loop, ev_timer *w, int revents);
};

}

#endif