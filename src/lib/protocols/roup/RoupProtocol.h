#ifndef ROUPPROTOCOL__H
#define ROUPPROTOCOL__H

#include "RoupIO.h"

namespace Roup
{

class RoupProtocol
:public RoupIO
{
	public:
		RoupProtocol(CoreObject *core,uint16_t &port);
		~RoupProtocol();
		
		void OnSystemMessage(RoupSession *session,uint8_t *data, ssize_t &size);
		
		void UpdateReadTime(RoupSession *session);
		virtual void CheckDelivery(RoupSession *session, uint8_t *data,ssize_t &size) = 0;
		
		Event3<RoupSession*,uint8_t *,ssize_t> OnNewMessage;
};

}

#endif