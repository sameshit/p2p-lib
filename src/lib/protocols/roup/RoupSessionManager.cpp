#include "RoupSessionManager.h"

using namespace std;
using namespace Roup;

RoupSessionManager::RoupSessionManager(CoreObject *core,int &fd)
:_core(core),_fd(fd)
{

}

RoupSessionManager::~RoupSessionManager()
{
	while(_ipv4_sessions.size() > 0)
		DestroyIpv4Session(_ipv4_sessions.begin()->second);
}

RoupSession *RoupSessionManager::CreateIpv4Session(uint32_t &host,uint16_t &port)
{
	map<pair<uint32_t,uint16_t>,RoupSession *>::iterator it;
	pair<uint32_t,uint16_t> hp_pair = make_pair(host,port);
	RoupSession *session;
	
	it = _ipv4_sessions.find(hp_pair);
	assert(it == _ipv4_sessions.end());
	
	fast_new4(RoupSession,session,_core,host,port,_fd);
	_ipv4_sessions.insert(make_pair(hp_pair,session));
	
	session->OnDisconnect.Attach<RoupSessionManager>(this,&RoupSessionManager::DestroyIpv4Session);
	LOG<<"CREAT"<<std::endl;
}

RoupSession *RoupSessionManager::FindIpv4Session(uint32_t &host,uint16_t &port)
{
	map<pair<uint32_t,uint16_t>,RoupSession *>::iterator it;
	
	it = _ipv4_sessions.find(make_pair(host,port));
	if (it == _ipv4_sessions.end())
		return NULL;
	else
		return (*it).second;
}

void RoupSessionManager::DestroyIpv4Session(RoupSession *session)
{
	map<pair<uint32_t,uint16_t>,RoupSession *>::iterator it;
	
	it = _ipv4_sessions.find(make_pair(session->GetNetworkIpv4Host(),session->GetNetworkIpv4Port()));
	assert(it != _ipv4_sessions.end());
	
	_ipv4_sessions.erase(it);
	
	fast_delete(RoupSession,session);
	LOG<<"DESTR"<<std::endl;
}