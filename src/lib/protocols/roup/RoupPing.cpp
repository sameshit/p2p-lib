#include "RoupPing.h"

using namespace Roup;
using namespace std;

RoupPing::RoupPing(CoreObject *core,uint16_t &port)
:RoupDelivery(core,port)
{
		_ping_timer.data = this;
	 	ev_timer_init(&_ping_timer, RoupPing::PingTimeout, 1., 1.);
    	ev_timer_start(_core->GetLoop(), &_ping_timer);
}

RoupPing::~RoupPing()
{
		ev_timer_stop(_core->GetLoop(),&_ping_timer);
}

void RoupPing::PingTimeout(struct ev_loop *loop, ev_timer *w, int revents)
{
	RoupPing *manager = (RoupPing*)w->data;
	map<pair<uint32_t,uint16_t>,RoupSession*>::iterator it;
	RoupSession *session;
	MessageType ping_type = Ping;
	ssize_t zero_size = 0;
	
	for (it = manager->GetSessionManager()->_ipv4_sessions.begin(); 
		it != manager->GetSessionManager()->_ipv4_sessions.end();
		it ++)
	{
		session = (*it).second;
		session->SystemSend(ping_type,NULL,zero_size);
	}
}