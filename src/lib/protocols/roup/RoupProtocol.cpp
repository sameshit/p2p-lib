#include "RoupProtocol.h"

using namespace Roup;

RoupProtocol::RoupProtocol(CoreObject *core, uint16_t &port)
:RoupIO(core,port)
{

}

RoupProtocol::~RoupProtocol()
{

}

void RoupProtocol::OnSystemMessage(RoupSession *session, uint8_t *data, ssize_t &size)
{
	MessageType msg_type;

	if (size < 5)
	{
		LOG << "Invalid datagram received from "<<session->GetRealHost()<<":"<<session->GetRealPort()<<std::endl;
		session->Disconnect();
		return;
	}
	
	UpdateReadTime(session); // calling for RoupConnectionManagers
	
	msg_type = (MessageType)data[0];	
	
	switch(msg_type)
	{
		case Ping:
		break;
		case Delivery:
			CheckDelivery(session,data,size);
		break;
		case Disconnect:
			session->SetInactive();
		break;
		case Message:
			OnNewMessage(session,&data[5],size - 5);
		break;
		default:
			LOG << "Unknown message type received from "<<session->GetRealHost()<<":"<<session->GetRealPort()<<std::endl;
			session->Disconnect();
		break;
	}
}

inline void RoupProtocol::UpdateReadTime(RoupSession *session)
{
//	gettimeofday(&session->_read_time,NULL);
}