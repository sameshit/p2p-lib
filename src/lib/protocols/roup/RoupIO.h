#ifndef ROUPIO__H
#define ROUPIO__H

#include "RoupBind.h"
#include "RoupSessionManager.h"

#define ROUP_BUFFER_SIZE 65536

namespace Roup
{

class RoupIO
:public RoupBind
{
	public:
		RoupIO(CoreObject *core,uint16_t &port);
		~RoupIO();		
		
		void OnRead();
		void OnWrite();
		
		virtual void OnSystemMessage(RoupSession *session,uint8_t *data,ssize_t &size) = 0;
		
		inline RoupSessionManager* GetSessionManager() {return _manager;}
	private:
		RoupSessionManager *_manager;
		uint8_t _buffer[ROUP_BUFFER_SIZE];
};

}
#endif