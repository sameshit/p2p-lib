#include "RoupConnectionManager.h"

using namespace Roup;
using namespace std;
#include <queue>

RoupConnectionManager::RoupConnectionManager(CoreObject *core,uint16_t &port)
:RoupProtocol(core,port)
{
		_conn_timer.data = this;
	 	ev_timer_init(&_conn_timer, RoupConnectionManager::ConnectionTimeout, ROUP_CONNECTION_TIMEOUT, ROUP_CONNECTION_TIMEOUT);
    	ev_timer_start(_core->GetLoop(), &_conn_timer);
}

RoupConnectionManager::~RoupConnectionManager()
{
		ev_timer_stop(_core->GetLoop(),&_conn_timer);
}

void RoupConnectionManager::ConnectionTimeout(struct ev_loop *loop, ev_timer *w, int revents)
{
	RoupConnectionManager *manager = (RoupConnectionManager *)w->data;	
	struct timeval now;
	map <pair<uint32_t,uint16_t>,RoupSession*>::iterator ipv4_it;
	queue <RoupSession *> sessions_for_delete;
	RoupSession *session;
	CoreObject *_core = manager->_core;
	
	gettimeofday(&now,NULL);
	
	for (ipv4_it = manager->GetSessionManager()->_ipv4_sessions.begin();
		ipv4_it != manager->GetSessionManager()->_ipv4_sessions.end(); 
		ipv4_it ++)
	{
		session = (*ipv4_it).second;
		if (Utils::DiffTimeMilli(now,session->_read_time)	 > (suseconds_t)(ROUP_CONNECTION_TIMEOUT*1000))
			sessions_for_delete.push(session);
	}	
	
	// HERE SHOULD BE IPV6 STUFF
	
	while (sessions_for_delete.size() > 0)
	{
		session = sessions_for_delete.front();
		
		LOG << "Disconnecting (reason:timeout) session "<<session->GetRealHost()<<":"<<session->GetRealPort() << std::endl;
		manager->GetSessionManager()->DestroyIpv4Session(session);
		sessions_for_delete.pop();
	}
}