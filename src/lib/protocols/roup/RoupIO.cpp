#include "RoupIO.h"

using namespace Roup;

RoupIO::RoupIO(CoreObject *core,uint16_t &port)
:RoupBind(core,port)
{
	fast_new2(RoupSessionManager,_manager,core,_fd);
}

RoupIO::~RoupIO()
{
	fast_delete(RoupSessionManager,_manager);
}

void RoupIO::OnRead()
{
	ssize_t recvd_bytes;
    struct sockaddr_in in_addr;
    socklen_t in_len = sizeof(in_addr);
    RoupSession *session;

	while (true)
	{
    	recvd_bytes = recvfrom(_fd,_buffer,ROUP_BUFFER_SIZE,MSG_DONTWAIT,(struct sockaddr *)&in_addr,&in_len);

	    if (recvd_bytes == -1)
    	{
        	if (errno != EAGAIN && errno != EWOULDBLOCK)
            	LOG << "Error while recving data" << std::endl;
	        break;
    	}
    
	    if (in_len == 0)
	    {
    	    LOG << "Bad recipent, "<<recvd_bytes<<" bytes received"<<std::endl;
			continue;
		}
    
	    session = _manager->FindIpv4Session(in_addr.sin_addr.s_addr, in_addr.sin_port);    
    
	    if (session == NULL)
	        session = _manager->CreateIpv4Session(in_addr.sin_addr.s_addr, in_addr.sin_port);

		OnSystemMessage(session,_buffer,recvd_bytes);
	}
}

void RoupIO::OnWrite(){assert(false);}