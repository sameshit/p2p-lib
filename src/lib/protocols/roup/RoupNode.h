#ifndef ROUPNODE__H
#define ROUPNODE__H

#include "RoupPing.h"

namespace Roup
{
	
class RoupNode
:public RoupPing
{
	public:
		RoupNode(CoreObject *core,uint16_t port);
		~RoupNode();
		
		RoupSession* ConnectIpv4(const char *host,uint16_t port) ;				
};
	
}

#endif