#include "RoupBind.h"

using namespace Roup;

RoupBind::RoupBind(CoreObject *core,uint16_t &port)
:UDPSocket(core)
{
	_addr.sin_family = AF_INET;
	_addr.sin_port = htons( port );
	_addr.sin_addr.s_addr = htonl( INADDR_ANY );
	
	if(bind(_fd, (struct sockaddr *)&_addr, sizeof(_addr)) != 0)
    {
        LOG<< "Couldn't bind socket on "<<inet_ntoa(_addr.sin_addr)<< ":" <<port << std::endl;
        exit(1);
    }
}

RoupBind::~RoupBind()
{

}