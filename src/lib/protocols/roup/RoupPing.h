#ifndef ROUPPING__H
#define ROUPPING__H

#include "RoupDelivery.h"

namespace Roup
{

class RoupPing
:public RoupDelivery
{
	public:
		RoupPing(CoreObject *core,uint16_t &port);
		~RoupPing();
	private:
		ev_timer _ping_timer;
	    static void PingTimeout (struct ev_loop *loop, ev_timer *w, int revents);						
};

}

#endif