#ifndef ROUPBIND__H
#define ROUPBIND__H

#include "../../core/protocols/UDP/UDPSocket.h"

namespace Roup
{

class RoupBind
:public UDPSocket
{
	public:	
		RoupBind(CoreObject *core,uint16_t &port);
		~RoupBind();
	private:
	    struct sockaddr_in _addr;
};

}
#endif