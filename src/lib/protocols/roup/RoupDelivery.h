#ifndef ROUPDELIVERY__H
#define ROUPDELVIERY__H

#include "RoupConnectionManager.h"

namespace Roup
{

class RoupDelivery
:public RoupConnectionManager
{
	public:
		RoupDelivery(CoreObject *core,uint16_t &port);
		~RoupDelivery();
		
		void CheckDelivery(RoupSession *session,uint8_t *data,ssize_t &size);
	private:
		ev_timer _delivery_timer;
	    static void DeliveryTimeout (struct ev_loop *loop, ev_timer *w, int revents);				
};

}

#endif