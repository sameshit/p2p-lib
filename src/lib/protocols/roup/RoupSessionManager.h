#ifndef ROUPSESSIONMANAGER__H
#define ROUPSESSIONMANAGER__H

#include "session/RoupSession.h"

namespace Roup
{

class RoupSessionManager
{
	public:
		RoupSessionManager(CoreObject *core,int &fd);
		~RoupSessionManager();
		
		// ipv4 stuff		
		RoupSession* 	CreateIpv4Session		(uint32_t &host,uint16_t &port);		
		void			DestroyIpv4Session		(RoupSession *session);
		RoupSession*	FindIpv4Session			(uint32_t &host,uint16_t &port);

		std::map< std::pair<uint32_t,uint16_t>, RoupSession *> _ipv4_sessions;		

	private:
		CoreObject *_core;		
		int _fd;
};

}
#endif