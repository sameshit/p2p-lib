#include "RoupSession.h"

using namespace Roup;

RoupNetworkSession::RoupNetworkSession(CoreObject *core,uint32_t &ipv4_host,uint16_t &ipv4_port,int &fd)
:_core(core),_ipv4(true),_fd(fd)
{	
    _addr.sin_port        = ipv4_port;
    _addr.sin_addr.s_addr = ipv4_host;
    _addr.sin_family      = AF_INET;
}

RoupNetworkSession::~RoupNetworkSession()
{

}

