#ifndef ROUPNETWORKSESSION__H
#define ROUPNETWORKSESSION__H

#include "../../../core/CoreObject.h"

namespace Roup
{

class RoupNetworkSession
{
	public:
		RoupNetworkSession(CoreObject *core,uint32_t &ipv4_host,uint16_t &ipv4_port,int &fd);
		~RoupNetworkSession();
		
		bool IsIpV4();
		
		inline uint32_t GetNetworkIpv4Host() {return _addr.sin_addr.s_addr;}
		inline uint16_t GetNetworkIpv4Port() {return _addr.sin_port;}
		
		inline const char* GetRealIpv4Host() {return inet_ntoa(_addr.sin_addr);}
		inline uint16_t	   GetRealIpv4Port() {return ntohs(_addr.sin_port);}
		
		// uinversal version, for ipv6 compability
        inline const char *GetRealHost() { if(_ipv4) return GetRealIpv4Host(); else return NULL;}
        inline uint16_t    GetRealPort() {if(_ipv4) return GetRealIpv4Port(); else return 0;}
		
		void Disconnect();		
	protected:
		CoreObject *_core;
	    struct sockaddr_in _addr;    				
		int _fd;	    
	private:			
		
		bool _ipv4;
};

}
#endif