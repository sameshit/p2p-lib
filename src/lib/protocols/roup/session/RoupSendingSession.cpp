#include "RoupSendingSession.h"

using namespace Roup;
using namespace std;

RoupSendingSession::RoupSendingSession(CoreObject *core,uint32_t &host,uint16_t &port,int &fd)
:RoupNetworkSession(core,host,port,fd),_last_sent_id(0),_active(true)
{
    _addr.sin_port        = port;
    _addr.sin_addr.s_addr = host;
    _addr.sin_family      = AF_INET;
}

RoupSendingSession::~RoupSendingSession()
{
	map<uint32_t,pair<struct timeval,pair<uint8_t *,ssize_t> > >::iterator it;
	uint8_t *data;
	
	for (it = _message_window.begin(); it != _message_window.end(); it ++)
	{
		data = (*it).second.second.first;
		fast_free(data);
	}
}

void RoupSendingSession::SystemSend(MessageType &type,uint8_t *data,ssize_t &size)
{
	uint8_t *new_data;
	struct timeval now;
	ssize_t sent_bytes,total_size = size +5;	
	
	if(!_active)
		return;
	
	new_data = fast_alloc(uint8_t,total_size);
	
	new_data[0] = (uint8_t)type;
	
	_last_sent_id++;
	Utils::PutBe32(&new_data[1],_last_sent_id);
	
	if (size != 0)
		memcpy(&new_data[5],data,size);
	
	gettimeofday(&now,NULL);
	
	_message_window.insert(make_pair(_last_sent_id,make_pair(now,make_pair(new_data,total_size))));
	
	sent_bytes = sendto(_fd,new_data,total_size,MSG_DONTWAIT,(struct sockaddr*)&_addr,sizeof(_addr));        
    assert (sent_bytes == total_size);
}

void RoupSendingSession::Send(uint8_t *data,ssize_t size)
{
	MessageType msg_type = Message;
	SystemSend(msg_type,data,size);
}

void RoupSendingSession::Resend(uint8_t *data, ssize_t &size)
{
	struct timeval now;
	ssize_t sent_bytes;

	if(!_active)
		return;

	_last_sent_id ++;
	Utils::PutBe32(&data[1],size);
	
	gettimeofday(&now,NULL);
	
	_message_window.insert(make_pair(_last_sent_id,make_pair(now,make_pair(data,size))));
	
	sent_bytes = sendto(_fd,data,size,MSG_DONTWAIT,(struct sockaddr*)&_addr,sizeof(_addr));        
    assert (sent_bytes == size);
}