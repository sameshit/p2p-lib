#ifndef ROUPSESSION__H
#define ROUPSESSION__H

#include "RoupSendingSession.h"

namespace Roup
{

class RoupSession
:public RoupSendingSession
{
	public:
		RoupSession(CoreObject *core,uint32_t &host,uint16_t &port,int &fd);
		~RoupSession();

		void Disconnect();
		inline void SetInactive() {_active = false;}
	private:			
		Event1<RoupSession*> OnDisconnect;		
		
		struct timeval _read_time;
		
		suseconds_t _sum_rtt;
		uint32_t _rtt_cnt;
		
		friend class RoupSessionManager;
		friend class RoupConnectionManager;
        friend class RoupProtocol;
		friend class RoupDelivery;
};

}

#endif