#ifndef ROUPSENDINGSESSION__H
#define ROUPSENDINGSESSION__H

#include "RoupNetworkSession.h"
#include "../../../utils/misc/Utils.h"

namespace Roup
{

enum MessageType
{
	Ping = 1,
	Delivery = 2,
	Message = 3,
	Disconnect = 4,
};

class RoupSendingSession
:public RoupNetworkSession
{
	public:
		RoupSendingSession(CoreObject *core,uint32_t &host,uint16_t &port,int &fd);
		~RoupSendingSession();
			
			
		void Send(uint8_t *data,ssize_t size);			
	private:
		void Resend(uint8_t *data,ssize_t &size);
		uint32_t _last_sent_id;
		
		std::map<uint32_t, std::pair<struct timeval,std::pair<uint8_t *,ssize_t> > > _message_window;
		
		struct sockaddr_in _addr;		
		
		friend class RoupDelivery;
		friend class RoupPing;
	protected:
		bool _active;
        void SystemSend(MessageType &type,uint8_t *data, ssize_t &size);
};

}

#endif