#include "RoupSession.h"

using namespace Roup;

RoupSession::RoupSession(CoreObject *core,uint32_t &host,uint16_t &port,int &fd)
:RoupSendingSession(core,host,port,fd),_sum_rtt(0),_rtt_cnt(0)
{
	gettimeofday(&_read_time,NULL);
}

RoupSession::~RoupSession()
{

}

void RoupSession::Disconnect()
{
	MessageType type = Roup::Disconnect;
	ssize_t size = 0;
	
	SystemSend(type,NULL,size);
	
	_active = false;
}