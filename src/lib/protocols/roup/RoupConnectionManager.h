#ifndef ROUPCONNECTIONMANAGER__H
#define ROUPCONNECTIONMANAGER__H

#include "RoupProtocol.h"
#include "../../utils/misc/Utils.h"

#define ROUP_CONNECTION_TIMEOUT 2.

namespace Roup
{

class RoupConnectionManager
:public RoupProtocol
{
	public:
		RoupConnectionManager(CoreObject *core,uint16_t &port);
		~RoupConnectionManager();
		
	private:		
		ev_timer _conn_timer;
	    static void ConnectionTimeout (struct ev_loop *loop, ev_timer *w, int revents);		
};

}

#endif