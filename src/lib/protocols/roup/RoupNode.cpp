#include "RoupNode.h"

using namespace Roup;

RoupNode::RoupNode(CoreObject *core,uint16_t port)
:RoupPing(core,port)
{

}

RoupNode::~RoupNode()
{

}

RoupSession* RoupNode::ConnectIpv4(const char* host,uint16_t port)
{
	uint32_t net_host;
	uint16_t net_port;
	
	net_host = inet_addr(host);
	net_port = htons(port);
	
	return GetSessionManager()->CreateIpv4Session(net_host,net_port);
}