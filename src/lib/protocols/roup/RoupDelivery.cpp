#include "RoupDelivery.h"
#include <queue>

using namespace Roup;
using namespace std;


RoupDelivery::RoupDelivery(CoreObject *core,uint16_t &port)
:RoupConnectionManager(core,port)
{
		_delivery_timer.data = this;
	 	ev_timer_init(&_delivery_timer, RoupDelivery::DeliveryTimeout, 1., 1.);
    	ev_timer_start(_core->GetLoop(), &_delivery_timer);
}

RoupDelivery::~RoupDelivery()
{
		ev_timer_stop(_core->GetLoop(),&_delivery_timer);
}

void RoupDelivery::CheckDelivery(RoupSession *session,uint8_t *data, ssize_t &size)
{
	uint32_t id;		
	map<uint32_t,pair<struct timeval,pair<uint8_t *,ssize_t> > >::iterator it;
	uint8_t *msg_data;
	struct timeval now;

	if (size != 5)
	{
		LOG << "Invalid delivery message size from "<<session->GetRealHost()<<":"<<session->GetRealPort()<< std::endl;
		session->Disconnect();
	}
	
	id = Utils::GetBe32(&data[1]);
	
	it = session->_message_window.find(id);
	
	if (it == session->_message_window.end())
	{
		LOG << "Received unknown delivery id(probably duplicated) from "<<session->GetRealHost()<<":"<<session->GetRealPort() << std::endl;
		return;
	}
	
	// calc rtt
	gettimeofday(&now,NULL);
	session->_sum_rtt += Utils::DiffTimeMilli(now,(*it).second.first);
	session->_rtt_cnt ++;
	
	msg_data = (*it).second.second.first;	
	fast_free(msg_data);
	
	session->_message_window.erase(it);
}


void RoupDelivery::DeliveryTimeout (struct ev_loop *loop, ev_timer *w, int revents)
{
	RoupDelivery *manager = (RoupDelivery *)w->data;	
	map<uint32_t,pair<struct timeval,pair<uint8_t*,ssize_t> > >::iterator msg_it;		
	map<pair<uint32_t,uint16_t>,RoupSession *>::iterator ses_it;
	RoupSession *session;
	suseconds_t rtt;
	struct timeval now;
	queue<pair<uint32_t,RoupSession *> > delete_queue;
	ssize_t size;
	uint8_t *data;
	uint32_t id;
	CoreObject *_core = manager->_core;
	
	gettimeofday(&now,NULL);
	
	for (ses_it = manager->GetSessionManager()->_ipv4_sessions.begin(); 
		ses_it != manager->GetSessionManager()->_ipv4_sessions.end(); 
		ses_it ++)
	{
		session = (*ses_it).second;
        if (session->_rtt_cnt != 0)
            rtt = session->_sum_rtt / session->_rtt_cnt;
        else
            rtt = 1000;
		
		LOG << "DEBUG: avg rtt is "<<rtt<<" for "<< session->GetRealHost() <<":" << session->GetRealPort() << std::endl;
		
		for (msg_it = session->_message_window.begin(); msg_it != session->_message_window.end(); msg_it ++)
		{
			if (Utils::DiffTimeMilli(now,(*msg_it).second.first) > 2*rtt)
			{
				LOG << "DEBUG: lost message("<<(*msg_it).first<<"), resending it to "<< session->GetRealHost() <<":" << session->GetRealPort() << std::endl;
					
				delete_queue.push(make_pair((*msg_it).first,session));	
				
				data = (*msg_it).second.second.first;
				size = (*msg_it).second.second.second;
				
				session->Resend(data,size);
			}
		}
	}
	
	while (delete_queue.size() > 0)
	{
		session = delete_queue.front().second;
		id = delete_queue.front().first;
		
		session->_message_window.erase(id);
        delete_queue.pop();
	}
}