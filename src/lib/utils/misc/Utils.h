/* 
 * File:   Utils.h
 * Author: v0id
 *
 * Created on 3 січня 2011, 23:54
 */

#ifndef UTILS_H
#define	UTILS_H

#include "../../common/config.h"

class Utils
{
public:
    static uint16_t GetBe16(uint8_t *data)
    {
          uint16_t val;
          val =  data[0]  << 8;
          val |= data[1];
          return val;
    }
    
    static uint32_t GetBe32(uint8_t *data)
    {
        uint32_t val;
        val =  GetBe16(data) << 16;
        val |= GetBe16(&data[2]);
        return val;
    }

    static uint64_t GetBe64(uint8_t *data )
    {
          uint64_t val;
          val =  (uint64_t)GetBe32(data) << 32;
          val |= (uint64_t)GetBe32(&data[4]);
          return val;
    }
    static void PutBe16(uint8_t *data,uint16_t val)
    {
	  data[0] = val >> 8;
	  data[1] = val;
    }
    static void PutBe32(uint8_t* data,uint32_t val)
    {
          data[0] = val >> 24;
          data[1] = val >> 16;
          data[2] = val >> 8;
          data[3] = val;
    }
    static void PutBe64(uint8_t* data,uint64_t val)
    {
          PutBe32(data,     (uint32_t)(val >> 32));
          PutBe32(&data[4], (uint32_t)(val & 0xffffffff));
    }
    static void PutByte(uint8_t* data,int val)
    {
        data[0] = val;
    }

    static int GetByte(uint8_t* data)
    {
        return data[0];
    }

    static uint8_t GetTrueBit(int pos)
    {
        uint8_t ret_val;
        switch (pos)
        {
            case 0:
                ret_val = 1;
            break;
            case 1:
                ret_val = 2;
            break;
            case 2:
                ret_val = 4;
            break;
            case 3:
                ret_val = 8;
            break;
            case 4:
                ret_val = 16;
            break;
            case 5:
                ret_val = 32;
            break;
            case 6:
                ret_val = 64;
            break;
            case 7:
                ret_val = 128;
            break;
            default:
                assert(false);
        }
        return ret_val;
    }
    
    static inline char* GetRealHost(uint32_t ip)
    {
        struct sockaddr_in _addr;
        
        _addr.sin_addr.s_addr = ip;
        _addr.sin_family      = AF_INET;
        
        return inet_ntoa(_addr.sin_addr);
    }
    static inline uint16_t  GetRealPort(uint16_t port)    
    {
        return ntohs(port);
    }

	static inline suseconds_t DiffTimeMilli(struct timeval &time1,struct timeval &time2)
	{
		return (time1.tv_sec-time2.tv_sec)*1000 + (time1.tv_usec - time2.tv_usec)/1000;
	}
};


#endif	/* UTILS_H */

